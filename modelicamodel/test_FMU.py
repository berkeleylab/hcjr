# Open license info at dymola and copy the address
# export DYMOLA_RUNTIME_LICENSE='/home/adun6414/.dassaultsystemes/dymola/dymola.lic'
# printenv

# In[1]:


import os
from matplotlib.pyplot import *
from numpy import *
from pandas import *
import sys


def res2df(res,varlist):
    data=list()
    for k in varlist:
        data.append(res[k])
    df=DataFrame(data=vstack(data).T,columns=varlist)
    return df

# try: 
#     sys.path.remove('/home/adun6414/JModelica/v2_0/JModelica/Python')
#     sys.path.append('/home/adun6414/Downloads/PyFMI')
# except:
#     print(sys.path)

try:
    os.environ['DYMOLA_RUNTIME_LICENSE']
except:
    os.environ['DYMOLA_RUNTIME_LICENSE']='/home/adun6414/.dassaultsystemes/dymola/dymola.lic'
    
import pyfmi

print(os.environ)

# In[2]:

current_folder=os.getcwd()
myfmu=os.path.join(current_folder,'modelicamodel_ModelWithHVACDX_0VariablePMV.fmu')
# myfmu=os.path.join(current_folder,'Buildings_Utilities_Comfort_Validation_FangerCBE.fmu')

fmuinpy=pyfmi.load_fmu(myfmu)

res=fmuinpy.simulate(180*3600*24,181*3600*24)
print(res.keys())


# In[ ]:
PMVvar=[k for k in res.keys() if 'Comfort.PMV' in k]
df=res2df(res,PMVvar)
figure(figsize=(10,5))
df.plot()

vars=['DX.P', 'DX.QSen_flow', 'DX.QLat_flow','SP_TSA.y', 'TS.T', 'TR.T','DX.TConIn']
df=res2df(res,vars)
figure(figsize=(10,5))
subplot(211)
df['DX.P'].plot()
subplot(212)
df[['TS.T','TR.T','DX.TConIn']].apply(lambda x:x-273.15).plot()

# os.system('ipynb-py-convert test_FMU.py test_FMU.ipynb')
