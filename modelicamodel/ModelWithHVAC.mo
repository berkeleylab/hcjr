within modelicamodel;
model ModelWithHVAC
  "From \"Buildings.ThermalZones.Detailed.Validation.BESTEST.Cases6xx.MPC_Fundamental.VAVsystem.Case600VAV.Case600FF_io_thickmass_Conv_Sizing_Tuning\""
  extends Modelica.Icons.Example;

  package MediumA = Buildings.Media.Air "Medium model";
  package MediumW = Buildings.Media.Water;
  // AHU modification ==================================

  constant Real conv=1.2/3600 "Conversion factor for nominal mass flow rate";
  //parameter Modelica.SIunits.MassFlowRate mRoo_flow_nominal=6*roo.AFlo*conv
  //  "Design mass flow rate core";
  //parameter Modelica.SIunits.MassFlowRate m_flow_nominal=0.7*mRoo_flow_nominal "Nominal mass flow rate";
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal=0.4 "Nominal mass flow rate";
  //Heating: 4kW (20oX) , Cooling:8kW(22oC), 300cfm/ton~0.05m3/s/kW --> 0.4kg/s (cf 0.068)


  parameter Modelica.SIunits.PressureDifference dpBuiStaSet(min=0) = 12
    "Building static pressure";
  parameter Real yFanMin = 0.1 "Minimum fan speed";

  parameter Boolean allowFlowReversal=true
    "= false to simplify equations, assuming, but not enforcing, no flow reversal";
  parameter Boolean sampleModel=true;

  // end AHU modification ==================================

  parameter Modelica.SIunits.Angle S_=
    Buildings.Types.Azimuth.S "Azimuth for south walls";
  parameter Modelica.SIunits.Angle E_=
    Buildings.Types.Azimuth.E "Azimuth for east walls";
  parameter Modelica.SIunits.Angle W_=
    Buildings.Types.Azimuth.W "Azimuth for west walls";
  parameter Modelica.SIunits.Angle N_=
    Buildings.Types.Azimuth.N "Azimuth for north walls";
  parameter Modelica.SIunits.Angle C_=
    Buildings.Types.Tilt.Ceiling "Tilt for ceiling";
  parameter Modelica.SIunits.Angle F_=
    Buildings.Types.Tilt.Floor "Tilt for floor";
  parameter Modelica.SIunits.Angle Z_=
    Buildings.Types.Tilt.Wall "Tilt for wall";
  parameter Integer nConExtWin = 1 "Number of constructions with a window";
  parameter Integer nConBou = 1
    "Number of surface that are connected to constructions that are modeled inside the room";

  Buildings.Fluid.Sources.Outside souInf(redeclare package Medium = MediumA,
      nPorts=2) "Source model for air infiltration"
           annotation (Placement(transformation(extent={{-388,-28},{-376,-16}})));

  Modelica.Blocks.Sources.Clock clock
    annotation (Placement(transformation(extent={{-246,-124},{-236,-114}})));
  measured_dis_Model
    SC annotation (Placement(transformation(extent={{-230,-130},{-210,-110}})));
  Buildings.Examples.VAVReheat.BaseClasses.MixingBox
    eco(
    redeclare package Medium = MediumA,
    mOut_flow_nominal=m_flow_nominal,
    dpOut_nominal=10,
    mRec_flow_nominal=m_flow_nominal,
    dpRec_nominal=10,
    mExh_flow_nominal=m_flow_nominal,
    dpExh_nominal=10,
    from_dp=false) "Economizer" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-318,-18})));
  Buildings.Fluid.Sensors.VolumeFlowRate mO(redeclare package Medium = MediumA,
      m_flow_nominal=m_flow_nominal) "Outside air volume flow rate"
    annotation (Placement(transformation(extent={{-364,-54},{-342,-32}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TM(
    redeclare package Medium = MediumA,
    m_flow_nominal=m_flow_nominal,
    allowFlowReversal=allowFlowReversal) "Mixed air temperature sensor"
    annotation (Placement(transformation(extent={{-284,-188},{-264,-168}})));
  Buildings.Fluid.HeatExchangers.DryCoilEffectivenessNTU heaCoi(
    show_T=true,
    redeclare package Medium1 = MediumW,
    redeclare package Medium2 = MediumA,
    m1_flow_nominal=m_flow_nominal*1000*(10 - (-20))/4200/10,
    m2_flow_nominal=m_flow_nominal,
    configuration=Buildings.Fluid.Types.HeatExchangerConfiguration.CounterFlow,
    Q_flow_nominal=m_flow_nominal*1006*(16.7 - 8.5),
    dp1_nominal=0,
    dp2_nominal=200 + 200 + 100 + 40,
    allowFlowReversal1=false,
    allowFlowReversal2=allowFlowReversal,
    T_a1_nominal=318.15,
    T_a2_nominal=281.65) "Heating coil"
    annotation (Placement(transformation(extent={{-234,-174},{-254,-194}})));

  Buildings.Fluid.Sources.Boundary_pT sinHea(
    redeclare package Medium = MediumW,
    p=300000,
    T=318.15,
    nPorts=1) "Sink for heating coil" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-262,-248})));
  Buildings.Fluid.Sources.MassFlowSource_T souHea(
    redeclare package Medium = MediumW,
    T=318.15,
    use_m_flow_in=true,
    nPorts=1) "Source for heating coil" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-228,-258})));
  Buildings.Fluid.Sources.Boundary_pT sinCoo(
    redeclare package Medium = MediumW,
    p=300000,
    T=285.15,
    nPorts=1) "Sink for cooling coil" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-174,-244})));
  Buildings.Fluid.HeatExchangers.WetCoilCounterFlow cooCoi(
    show_T=true,
    UA_nominal=3*m_flow_nominal*1000*15/
        Buildings.Fluid.HeatExchangers.BaseClasses.lmtd(
        T_a1=26.2,
        T_b1=12.8,
        T_a2=6,
        T_b2=16),
    redeclare package Medium1 = MediumW,
    redeclare package Medium2 = MediumA,
    m1_flow_nominal=m_flow_nominal*1000*15/4200/10,
    m2_flow_nominal=m_flow_nominal,
    dp2_nominal=0,
    dp1_nominal=0,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    allowFlowReversal1=false,
    allowFlowReversal2=allowFlowReversal) "Cooling coil"
    annotation (Placement(transformation(extent={{-148,-174},{-168,-194}})));
  Buildings.Fluid.Sources.MassFlowSource_T souCoo(
    redeclare package Medium = MediumW,
    T=279.15,
    nPorts=1,
    use_m_flow_in=true) "Source for cooling coil" annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-124,-244})));
  Buildings.Fluid.Movers.FlowControlled_m_flow fanSup(
    redeclare package Medium = MediumA,
    m_flow_nominal=m_flow_nominal,
    per(pressure(V_flow={0,m_flow_nominal/1.2*2}, dp=2*{780 + 10 + dpBuiStaSet,
            0})),
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    addPowerToMedium=false) "Supply air fan"
    annotation (Placement(transformation(extent={{-98,-188},{-78,-168}})));

  Buildings.Fluid.Sensors.TemperatureTwoPort TS(
    redeclare package Medium = MediumA,
    m_flow_nominal=m_flow_nominal,
    allowFlowReversal=allowFlowReversal)
    annotation (Placement(transformation(extent={{-40,-188},{-20,-168}})));
  Buildings.Fluid.Sensors.VolumeFlowRate mS(redeclare package Medium = MediumA,
      m_flow_nominal=m_flow_nominal) "Sensor for supply fan flow rate"
    annotation (Placement(transformation(extent={{30,-188},{50,-168}})));
  Modelica.Blocks.Sources.RealExpression yOA(y=0.2)
    annotation (Placement(transformation(extent={{-390,78},{-368,100}})));
  Modelica.Blocks.Math.Add add(k2=-1)
    annotation (Placement(transformation(extent={{-332,94},{-312,114}})));
  Modelica.Blocks.Sources.RealExpression realExpression6(y=1)
    annotation (Placement(transformation(extent={{-390,108},{-370,128}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TSh(
    redeclare package Medium = MediumA,
    m_flow_nominal=m_flow_nominal,
    allowFlowReversal=allowFlowReversal) "Mixed air temperature sensor"
    annotation (Placement(transformation(extent={{-216,-188},{-196,-168}})));
  Modelica.Blocks.Sources.RealExpression realExpression1(y=0)
    annotation (Placement(transformation(extent={{-312,-298},{-292,-278}})));
  Buildings.Controls.OBC.CDL.Continuous.Gain gaiCooCoi1(k=10*m_flow_nominal*
        1000*15/4200/10) "Gain for cooling coil mass flow rate"
    annotation (Placement(transformation(extent={{-90,-328},{-70,-308}})));
  Modelica.Blocks.Math.Add add1(k2=-1)
    annotation (Placement(transformation(extent={{-186,-336},{-166,-316}})));
  Modelica.Blocks.Sources.RealExpression realExpression2(y=273.15)
    annotation (Placement(transformation(extent={{-236,-342},{-216,-322}})));
  Modelica.Blocks.Math.Add add2(k2=-1)
    annotation (Placement(transformation(extent={{-120,-386},{-140,-366}})));
  Modelica.Blocks.Sources.RealExpression realExpression3(y=273.15)
    annotation (Placement(transformation(extent={{-66,-402},{-86,-382}})));
  Buildings.Controls.Continuous.LimPID
                                    cooCoiCon(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=0.1,
    Ti=200,
    Td=0,
    yMax=1,
    yMin=0,
    initType=Modelica.Blocks.Types.InitPID.InitialState,
    reverseActing=false)
    annotation (Placement(transformation(extent={{-132,-328},{-112,-308}})));
  Modelica.Blocks.Continuous.LimPID Thermostat(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=0.1,
    Ti=100,
    Td=0,
    yMax=1,
    yMin=1e-2,
    initType=Modelica.Blocks.Types.InitPID.InitialState)
    annotation (Placement(transformation(extent={{-136,-150},{-116,-130}})));
  Buildings.Controls.OBC.CDL.Continuous.Gain gaiFanFlo(k=m_flow_nominal)
    annotation (Placement(transformation(extent={{-94,-152},{-74,-132}})));
  Modelica.Blocks.Sources.Pulse pulse(
    amplitude=5,
    width=50,
    period(displayUnit="d") = 86400,
    offset=273.15 + 10)
    annotation (Placement(transformation(extent={{-282,-330},{-262,-310}})));
  Modelica.Blocks.Continuous.StateSpace LPF(
    A=[-0.01],
    B=[0.01],
    C=[1],
    D=[0],
    initType=Modelica.Blocks.Types.Init.InitialState,
    x_start={293.15})
    annotation (Placement(transformation(extent={{-208,-144},{-188,-124}})));
  BaseClasses.PartialModel_Envelope partialModel_Envelope
    annotation (Placement(transformation(extent={{-72,-78},{30,0}})));
  Modelica.Blocks.Sources.RealExpression Pert1[2](y={0,0})
    annotation (Placement(transformation(extent={{-196,-42},{-156,-2}})));
  Modelica.Blocks.Sources.RealExpression QGai
    annotation (Placement(transformation(extent={{-222,-62},{-182,-22}})));
  MeasuredQ Qin_sen
    annotation (Placement(transformation(extent={{110,-286},{182,-214}})));
  Modelica.Blocks.Interfaces.RealOutput Q
    annotation (Placement(transformation(extent={{198,-266},{252,-212}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TR(
    redeclare package Medium = MediumA,
    m_flow_nominal=m_flow_nominal,
    allowFlowReversal=allowFlowReversal) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-168,42})));
  Modelica.Blocks.Sources.RealExpression FromTR(y=TR.T)
    annotation (Placement(transformation(extent={{40,-304},{80,-264}})));
  Modelica.Blocks.Sources.RealExpression FromTS(y=TS.T)
    annotation (Placement(transformation(extent={{38,-274},{78,-234}})));
equation

  connect(clock.y, SC.cur_t) annotation (Line(points={{-235.5,-119},{-231.75,
          -119},{-231.75,-118.2},{-226.2,-118.2}},
                                      color={0,0,127}));
  connect(souInf.ports[1], eco.port_Out) annotation (Line(points={{-376,-20.8},{
          -352,-20.8},{-352,-12},{-328,-12}}, color={0,127,255}));
  connect(souInf.ports[2], mO.port_a) annotation (Line(points={{-376,-23.2},{-368,
          -23.2},{-368,-43},{-364,-43}}, color={0,127,255}));
  connect(mO.port_b, eco.port_Exh) annotation (Line(points={{-342,-43},{-334,-43},
          {-334,-24},{-328,-24}}, color={0,127,255}));
  connect(TS.port_b, mS.port_a)
    annotation (Line(points={{-20,-178},{30,-178}}, color={0,127,255}));
  connect(eco.port_Ret, TM.port_a) annotation (Line(points={{-308,-24},{-294,-24},
          {-294,-178},{-284,-178}}, color={0,127,255}));
  connect(heaCoi.port_a2, TM.port_b)
    annotation (Line(points={{-254,-178},{-264,-178}}, color={0,127,255}));
  connect(cooCoi.port_b2, fanSup.port_a)
    annotation (Line(points={{-148,-178},{-98,-178}}, color={0,127,255}));
  connect(souCoo.ports[1], cooCoi.port_a1) annotation (Line(points={{-124,-234},
          {-136,-234},{-136,-190},{-148,-190}}, color={0,127,255}));
  connect(cooCoi.port_b1, sinCoo.ports[1]) annotation (Line(points={{-168,-190},
          {-172,-190},{-172,-234},{-174,-234}}, color={0,127,255}));
  connect(souHea.ports[1], heaCoi.port_a1) annotation (Line(points={{-228,-248},
          {-228,-190},{-234,-190}}, color={0,127,255}));
  connect(heaCoi.port_b1, sinHea.ports[1]) annotation (Line(points={{-254,-190},
          {-262,-190},{-262,-238}}, color={0,127,255}));
  connect(fanSup.port_b, TS.port_a)
    annotation (Line(points={{-78,-178},{-40,-178}}, color={0,127,255}));
  connect(realExpression6.y, add.u1) annotation (Line(points={{-369,118},{-348,
          118},{-348,110},{-334,110}}, color={0,0,127}));
  connect(heaCoi.port_b2,TSh. port_a)
    annotation (Line(points={{-234,-178},{-216,-178}}, color={0,127,255}));
  connect(TSh.port_b, cooCoi.port_a2)
    annotation (Line(points={{-196,-178},{-168,-178}}, color={0,127,255}));
  connect(realExpression1.y, souHea.m_flow_in) annotation (Line(points={{-291,
          -288},{-238,-288},{-238,-270},{-236,-270}}, color={0,0,127}));
  connect(gaiCooCoi1.y, souCoo.m_flow_in) annotation (Line(points={{-68,-318},{
          -52,-318},{-52,-256},{-132,-256}}, color={0,0,127}));
  connect(realExpression2.y, add1.u2) annotation (Line(points={{-215,-332},{
          -188,-332}},                         color={0,0,127}));
  connect(realExpression3.y, add2.u2) annotation (Line(points={{-87,-392},{-104,
          -392},{-104,-382},{-118,-382}}, color={0,0,127}));
  connect(TS.T, add2.u1) annotation (Line(points={{-30,-167},{-2,-167},{-2,-370},
          {-118,-370}}, color={0,0,127}));
  connect(cooCoiCon.y, gaiCooCoi1.u)
    annotation (Line(points={{-111,-318},{-92,-318}}, color={0,0,127}));
  connect(Thermostat.y, gaiFanFlo.u) annotation (Line(points={{-115,-140},{-106,
          -140},{-106,-142},{-96,-142}}, color={0,0,127}));
  connect(gaiFanFlo.y, fanSup.m_flow_in) annotation (Line(points={{-72,-142},{-72,
          -152},{-88,-152},{-88,-166}}, color={0,0,127}));
  connect(pulse.y, add1.u1) annotation (Line(points={{-261,-320},{-188,-320}},
                                   color={0,0,127}));
  connect(SC.Tu, LPF.u[1]) annotation (Line(points={{-219.4,-118.6},{-219.4,
          -125.3},{-210,-125.3},{-210,-134}}, color={0,0,127}));
  connect(LPF.y[1], Thermostat.u_m) annotation (Line(points={{-187,-134},{-182,
          -134},{-182,-152},{-126,-152}}, color={0,0,127}));
  connect(partialModel_Envelope.weaBus, souInf.weaBus) annotation (Line(
      points={{32.3538,-65.52},{84,-65.52},{84,140},{-408,140},{-408,-21.88},{
          -388,-21.88}},
      color={255,204,51},
      thickness=0.5));
  connect(mS.port_b, partialModel_Envelope.a) annotation (Line(points={{50,-178},
          {124,-178},{124,-102},{-122,-102},{-122,-3.12},{-71.6077,-3.12}},
        color={0,127,255}));
  connect(Pert1.y, partialModel_Envelope.Pert) annotation (Line(points={{-154,
          -22},{-114,-22},{-114,-23.4},{-72,-23.4}}, color={0,0,127}));
  connect(QGai.y, partialModel_Envelope.Q) annotation (Line(points={{-180,-42},
          {-126,-42},{-126,-39},{-72,-39}}, color={0,0,127}));
  connect(partialModel_Envelope.y, Thermostat.u_s) annotation (Line(points={{33.9231,
          -78},{170,-78},{170,-92},{-158,-92},{-158,-140},{-138,-140}},
        color={0,0,127}));
  connect(Qin_sen.Qo,Q)  annotation (Line(points={{174.8,-239.2},{192.4,-239.2},
          {192.4,-239},{225,-239}}, color={0,0,127}));
  connect(mS.V_flow, Qin_sen.V) annotation (Line(points={{40,-167},{40,-146},{
          68,-146},{68,-222},{119.36,-222},{119.36,-231.28}}, color={0,0,127}));
  connect(partialModel_Envelope.b, TR.port_a) annotation (Line(points={{30,
          -1.17},{48,-1.17},{48,42},{-158,42}}, color={0,127,255}));
  connect(TR.port_b, eco.port_Sup) annotation (Line(points={{-178,42},{-226,42},
          {-226,40},{-298,40},{-298,-12},{-308,-12}}, color={0,127,255}));
  connect(FromTR.y, Qin_sen.To) annotation (Line(points={{82,-284},{104,-284},{
          104,-277.36},{121.52,-277.36}}, color={0,0,127}));
  connect(FromTS.y, Qin_sen.Ti) annotation (Line(points={{80,-254},{102,-254},{
          102,-254.32},{120.08,-254.32}}, color={0,0,127}));
  connect(yOA.y, add.u2) annotation (Line(points={{-366.9,89},{-345.45,89},{
          -345.45,98},{-334,98}}, color={0,0,127}));
  connect(yOA.y, eco.yExh) annotation (Line(points={{-366.9,89},{-358,89},{-358,
          6},{-311,6},{-311,-6}}, color={0,0,127}));
  connect(add.y, eco.yRet) annotation (Line(points={{-311,104},{-296,104},{-296,
          66},{-324.8,66},{-324.8,-6}}, color={0,0,127}));
  connect(yOA.y, eco.yOut) annotation (Line(points={{-366.9,89},{-358,89},{-358,
          6},{-318,6},{-318,-6}}, color={0,0,127}));
  connect(add1.y, cooCoiCon.u_s) annotation (Line(points={{-165,-326},{-162,
          -326},{-162,-318},{-134,-318}}, color={0,0,127}));
  connect(add2.y, cooCoiCon.u_m) annotation (Line(points={{-141,-376},{-162,
          -376},{-162,-350},{-122,-350},{-122,-330}}, color={0,0,127}));
  annotation (
experiment(Tolerance=1e-06, StopTime=3.1536e+07),
__Dymola_Commands(file(inherit=true) = "ModelWithHVAC.mos"),
                              Documentation(info="<html>
<p>
This model is used for the test case 600FF of the BESTEST validation suite.
Case 600FF is a light-weight building.
The room temperature is free floating.
</p>
</html>", revisions="<html>
<ul>
<li>
October 29, 2016, by Michael Wetter:<br/>
Placed a capacity at the room-facing surface
to reduce the dimension of the nonlinear system of equations,
which generally decreases computing time.<br/>
Removed the pressure drop element which is not needed.<br/>
Linearized the radiative heat transfer, which is the default in
the library, and avoids a large nonlinear system of equations.<br/>
This is for
<a href=\"https://github.com/lbl-srg/modelica-buildings/issues/565\">issue 565</a>.
</li>
<li>
December 22, 2014 by Michael Wetter:<br/>
Removed <code>Modelica.Fluid.System</code>
to address issue
<a href=\"https://github.com/lbl-srg/modelica-buildings/issues/311\">#311</a>.
</li>
<li>
October 9, 2013, by Michael Wetter:<br/>
Implemented soil properties using a record so that <code>TSol</code> and
<code>TLiq</code> are assigned.
This avoids an error when the model is checked in the pedantic mode.
</li>
<li>
July 15, 2012, by Michael Wetter:<br/>
Added reference results.
Changed implementation to make this model the base class
for all BESTEST cases.
Added computation of hourly and annual averaged room air temperature.
<li>
October 6, 2011, by Michael Wetter:<br/>
First implementation.
</li>
</ul>
</html>"),
    Diagram(coordinateSystem(extent={{-440,-400},{200,140}})),
    Icon(coordinateSystem(extent={{-440,-400},{200,140}})));
end ModelWithHVAC;
