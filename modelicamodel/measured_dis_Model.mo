within modelicamodel;
model measured_dis_Model

  Modelica.Blocks.Interfaces.RealInput cur_t
    annotation (Placement(transformation(extent={{-82,-2},{-42,38}})));
  Modelica.Blocks.Interfaces.RealOutput Tl
    annotation (Placement(transformation(extent={{-6,36},{14,56}})));
  Modelica.Blocks.Interfaces.RealOutput Tu
    annotation (Placement(transformation(extent={{-4,4},{16,24}})));
  Modelica.Blocks.Interfaces.RealOutput R
    annotation (Placement(transformation(extent={{-2,-28},{18,-8}})));

protected
  outlist
    Y;
equation
  Y =
    modelicamodel.measured_disturbances(cur_t);
  Tl=Y.Tl;
  Tu=Y.Tu;
  R=Y.R;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Text(
          extent={{8,66},{28,48}},
          lineColor={28,108,200},
          textString="L"),
        Text(
          extent={{10,30},{30,12}},
          lineColor={28,108,200},
          textString="U"),
        Text(
          extent={{8,2},{28,-16}},
          lineColor={28,108,200},
          textString="R"),
        Rectangle(
          extent={{-68,72},{6,-34}},
          lineColor={28,108,200},
          fillColor={28,108,200},
          fillPattern=FillPattern.Solid)}),                      Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end measured_dis_Model;
