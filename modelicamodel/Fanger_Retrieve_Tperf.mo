within modelicamodel;
model Fanger_Retrieve_Tperf "Test of Comfort Model"
  extends Modelica.Icons.Example;
  Modelica.SIunits.Temperature TAirPMV0(start=273.15+20);
  Buildings.Utilities.Comfort.Fanger theCom(
    use_vAir_in=true,
    use_M_in=true,
    use_pAir_in=true) "Thermal comfort model"
                                            annotation (
      Placement(transformation(extent={{70,-40},{90,-20}})));
  Modelica.Blocks.Sources.Constant ICl(k=0.9) "Clothing insulation"
    annotation (Placement(transformation(extent={{-24,-98},{-4,-78}})));
  Modelica.Blocks.Sources.Constant vAir(k=0.05) "Air velocity"
    annotation (Placement(transformation(extent=[-80,-56; -60,-36])));
  Modelica.Blocks.Sources.Constant M(k=60) "Metabolic heat generated"
    annotation (Placement(transformation(extent=[-80,-86; -60,-66])));
  Modelica.Blocks.Sources.RealExpression
                                   TRad(y=273.15 + 22) "Radiation temperature"
    annotation (Placement(transformation(extent=[-80,-26; -60,-6])));
  Modelica.Blocks.Sources.Constant pAtm(k=101325)
    annotation (Placement(transformation(extent=[0,68; 20,88])));
  Modelica.Blocks.Sources.Constant phi(k=0.5) "Relative humidity"
    annotation (Placement(transformation(extent={{-20,
            30},{0,50}})));
  Modelica.Blocks.Sources.Ramp TAir1(
    duration=1,
    height=10,
    offset=273.15 + 20) "Air temperature"
                        annotation (Placement(
        transformation(extent={{-92,50},{-72,70}})));
  Buildings.Utilities.Comfort.Fanger theComEst(TAir=TAirPMV0,
    use_vAir_in=true,
    use_M_in=true,
    use_pAir_in=true) "Thermal comfort model"
    annotation (Placement(transformation(extent={{76,-88},{96,-68}})));
equation
  theComEst.PMV=0; //find an indoor air temperature which results in PMV=0.

  connect(pAtm.y, theCom.pAir_in) annotation (Line(
      points={{21,78},{30,78},{30,-40},{69,-40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRad.y, theCom.TRad) annotation (Line(
      points={{-59,-16},{8,-16},{8,-24},{69,-24}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(theCom.vAir_in, vAir.y) annotation (Line(
      points={{69,-31},{-28,-31},{-28,-46},{-59,-46}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(M.y, theCom.M_in) annotation (Line(
      points={{-59,-76},{-20,-76},{-20,-34},{69,-34}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(ICl.y, theCom.ICl_in) annotation (Line(
      points={{-3,-88},{20,-88},{20,-37},{69,-37}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(phi.y, theCom.phi) annotation (Line(
      points={{1,40},{60,40},{60,-28},{69,-28}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TAir1.y, theCom.TAir) annotation (Line(points={{-71,60},{-52,60},{-52,
          2},{69,2},{69,-20}}, color={0,0,127}));
  connect(TRad.y, theComEst.TRad) annotation (Line(
      points={{-59,-16},{44,-16},{44,-72},{75,-72}},
      color={170,255,255},
      thickness=0.5,
      pattern=LinePattern.Dash));
  connect(phi.y, theComEst.phi) annotation (Line(
      points={{1,40},{14,40},{14,38},{38,38},{38,-76},{75,-76}},
      color={170,255,255},
      thickness=0.5,
      pattern=LinePattern.Dash));
  connect(vAir.y, theComEst.vAir_in) annotation (Line(
      points={{-59,-46},{8,-46},{8,-79},{75,-79}},
      color={170,255,255},
      thickness=0.5,
      pattern=LinePattern.Dash));
  connect(M.y, theComEst.M_in) annotation (Line(
      points={{-59,-76},{8,-76},{8,-82},{75,-82}},
      color={170,255,255},
      thickness=0.5,
      pattern=LinePattern.Dash));
  connect(ICl.y, theComEst.ICl_in) annotation (Line(
      points={{-3,-88},{36,-88},{36,-85},{75,-85}},
      color={170,255,255},
      thickness=0.5,
      pattern=LinePattern.Dash));
  connect(pAtm.y, theComEst.pAir_in) annotation (Line(
      points={{21,78},{30,78},{30,-92},{75,-92},{75,-88}},
      color={170,255,255},
      thickness=0.5,
      pattern=LinePattern.Dash));
 annotation (experiment(Tolerance=1e-6, StopTime=1.0),
__Dymola_Commands(file(inherit=true) = "\"Fanger_Retrieve_Tperf.mos\""),
    Documentation(info="<html>
This is a test of the Thermal Comfort Model.
</html>"));
end Fanger_Retrieve_Tperf;
