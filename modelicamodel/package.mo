within ;
package modelicamodel "Library with modelica models for HCJR project"
  extends Modelica.Icons.Package;

annotation (uses(Modelica(version="3.2.2"), Buildings(version="8.0.0")));
end modelicamodel;
