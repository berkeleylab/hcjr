within modelicamodel;
model ModelWithHVACDX_VariablePMV
  "From \"Buildings.ThermalZones.Detailed.Validation.BESTEST.Cases6xx.MPC_Fundamental.VAVsystem.Case600VAV.Case600FF_io_thickmass_Conv_Sizing_Tuning\""
  extends Modelica.Icons.Example;

  package MediumA = Buildings.Media.Air "Medium model";
  package MediumW = Buildings.Media.Water;
  // AHU modification ==================================

  //constant Real conv=1.2/3600 "Conversion factor for nominal mass flow rate";
  //parameter Modelica.SIunits.MassFlowRate mRoo_flow_nominal=6*roo.AFlo*conv
  //  "Design mass flow rate core";
  //parameter Modelica.SIunits.MassFlowRate m_flow_nominal=0.7*mRoo_flow_nominal "Nominal mass flow rate";

  //Heating: 4kW (20oX) , Cooling:8kW(22oC), 300cfm/ton~0.1m3/s/ton--> 0.28 kg/s (cf 0.068)
  parameter Modelica.SIunits.MassFlowRate m_flow_nominal=0.28 "Nominal mass flow rate";
  parameter Modelica.SIunits.HeatFlowRate Qc_nominal=8000/0.8 "nominal cooling total cap [W]";
  parameter Modelica.SIunits.HeatFlowRate Qh_nominal=4000 "nominal heating cap [W]";

  parameter Modelica.SIunits.MassFlowRate mCor_flow_nominal=m_flow_nominal;
  parameter Real VRooCor=1;

  parameter Modelica.SIunits.PressureDifference dpBuiStaSet(min=0) = 12
    "Building static pressure";
  parameter Real yFanMin = 0.01 "Minimum fan speed";

  parameter Boolean allowFlowReversal=true
    "= false to simplify equations, assuming, but not enforcing, no flow reversal";
  parameter Boolean sampleModel=true;

  // end AHU modification ==================================

  parameter Modelica.SIunits.Angle S_=
    Buildings.Types.Azimuth.S "Azimuth for south walls";
  parameter Modelica.SIunits.Angle E_=
    Buildings.Types.Azimuth.E "Azimuth for east walls";
  parameter Modelica.SIunits.Angle W_=
    Buildings.Types.Azimuth.W "Azimuth for west walls";
  parameter Modelica.SIunits.Angle N_=
    Buildings.Types.Azimuth.N "Azimuth for north walls";
  parameter Modelica.SIunits.Angle C_=
    Buildings.Types.Tilt.Ceiling "Tilt for ceiling";
  parameter Modelica.SIunits.Angle F_=
    Buildings.Types.Tilt.Floor "Tilt for floor";
  parameter Modelica.SIunits.Angle Z_=
    Buildings.Types.Tilt.Wall "Tilt for wall";
  parameter Integer nConExtWin = 1 "Number of constructions with a window";
  parameter Integer nConBou = 1
    "Number of surface that are connected to constructions that are modeled inside the room";

  Buildings.Fluid.Sources.Outside souInf(redeclare package Medium = MediumA,
      nPorts=2) "Source model for air infiltration"
           annotation (Placement(transformation(extent={{-388,-28},{-376,-16}})));

  Modelica.Blocks.Sources.Clock clock
    annotation (Placement(transformation(extent={{-58,-80},{-48,-70}})));
  measured_dis_Model
    SC annotation (Placement(transformation(extent={{-42,-86},{-22,-66}})));
  Buildings.Examples.VAVReheat.BaseClasses.MixingBox
    eco(
    redeclare package Medium = MediumA,
    mOut_flow_nominal=m_flow_nominal,
    dpOut_nominal=10,
    mRec_flow_nominal=m_flow_nominal,
    dpRec_nominal=10,
    mExh_flow_nominal=m_flow_nominal,
    dpExh_nominal=10,
    from_dp=false) "Economizer" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={-318,-18})));
  Buildings.Fluid.Sensors.VolumeFlowRate mO(redeclare package Medium = MediumA,
      m_flow_nominal=m_flow_nominal) "Outside air volume flow rate"
    annotation (Placement(transformation(extent={{-364,-54},{-342,-32}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TM(
    redeclare package Medium = MediumA,
    m_flow_nominal=m_flow_nominal,
    allowFlowReversal=allowFlowReversal) "Mixed air temperature sensor"
    annotation (Placement(transformation(extent={{-284,-188},{-264,-168}})));
  Buildings.Fluid.HeatExchangers.DryCoilEffectivenessNTU heaCoi(
    show_T=true,
    redeclare package Medium1 = MediumW,
    redeclare package Medium2 = MediumA,
    m1_flow_nominal=m_flow_nominal*1000*(10 - (-20))/4200/10,
    m2_flow_nominal=m_flow_nominal,
    configuration=Buildings.Fluid.Types.HeatExchangerConfiguration.CounterFlow,
    Q_flow_nominal=m_flow_nominal*1006*(16.7 - 8.5),
    dp1_nominal=0,
    dp2_nominal=200 + 200 + 100 + 40,
    allowFlowReversal1=false,
    allowFlowReversal2=allowFlowReversal,
    T_a1_nominal=318.15,
    T_a2_nominal=281.65) "Heating coil"
    annotation (Placement(transformation(extent={{-234,-174},{-254,-194}})));

  Buildings.Fluid.Sources.Boundary_pT sinHea(
    redeclare package Medium = MediumW,
    p=300000,
    T=318.15,
    nPorts=1) "Sink for heating coil" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-262,-248})));
  Buildings.Fluid.Sources.MassFlowSource_T souHea(
    redeclare package Medium = MediumW,
    T=318.15,
    use_m_flow_in=true,
    nPorts=1) "Source for heating coil" annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-228,-258})));
  Buildings.Fluid.Movers.FlowControlled_dp     fanSup(
    redeclare package Medium = MediumA,
    m_flow_nominal=m_flow_nominal,
    per(pressure(V_flow={0,m_flow_nominal/1.2*2}, dp=2*{780 + 10 + dpBuiStaSet,
            0})),
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    inputType=Buildings.Fluid.Types.InputType.Continuous,
    addPowerToMedium=false) "Supply air fan"
    annotation (Placement(transformation(extent={{-98,-188},{-78,-168}})));

  Buildings.Fluid.Sensors.TemperatureTwoPort TS(
    redeclare package Medium = MediumA,
    m_flow_nominal=m_flow_nominal,
    allowFlowReversal=allowFlowReversal)
    annotation (Placement(transformation(extent={{-40,-188},{-20,-168}})));
  Buildings.Fluid.Sensors.VolumeFlowRate mS(redeclare package Medium = MediumA,
      m_flow_nominal=m_flow_nominal) "Sensor for supply fan flow rate"
    annotation (Placement(transformation(extent={{30,-188},{50,-168}})));
  Modelica.Blocks.Sources.RealExpression yOA(y=0.2)
    annotation (Placement(transformation(extent={{-390,78},{-368,100}})));
  Modelica.Blocks.Math.Add add(k2=-1)
    annotation (Placement(transformation(extent={{-332,94},{-312,114}})));
  Modelica.Blocks.Sources.RealExpression realExpression6(y=1)
    annotation (Placement(transformation(extent={{-390,108},{-370,128}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TSh(
    redeclare package Medium = MediumA,
    m_flow_nominal=m_flow_nominal,
    allowFlowReversal=allowFlowReversal) "Mixed air temperature sensor"
    annotation (Placement(transformation(extent={{-216,-188},{-196,-168}})));
  Modelica.Blocks.Sources.RealExpression realExpression1(y=0)
    annotation (Placement(transformation(extent={{-312,-298},{-292,-278}})));
  Modelica.Blocks.Math.Add add2(k2=-1)
    annotation (Placement(transformation(extent={{-120,-386},{-140,-366}})));
  Modelica.Blocks.Sources.RealExpression realExpression3(y=273.15)
    annotation (Placement(transformation(extent={{-66,-402},{-86,-382}})));
  Buildings.Controls.Continuous.LimPID
                                    cooCoiCon(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=0.1,
    Ti=200,
    Td=0,
    yMax=1,
    yMin=0,
    initType=Modelica.Blocks.Types.InitPID.InitialState,
    reverseActing=false)
    annotation (Placement(transformation(extent={{-132,-328},{-112,-308}})));
  Buildings.Controls.Continuous.LimPID
                                    Thermostat(
    controllerType=Modelica.Blocks.Types.SimpleController.PI,
    k=0.1,
    Ti=100,
    Td=0,
    yMax=1,
    yMin=3e-1,
    initType=Modelica.Blocks.Types.InitPID.InitialState,
    reverseActing=false)
    annotation (Placement(transformation(extent={{64,-86},{84,-66}})));
  Modelica.Blocks.Continuous.StateSpace LPF(
    A=[-0.01],
    B=[0.01],
    C=[1],
    D=[0],
    initType=Modelica.Blocks.Types.Init.InitialState,
    x_start={293.15})
    annotation (Placement(transformation(extent={{-8,-86},{12,-66}})));
  BaseClasses.PartialModel_Envelope_V3
                                    partialModel_Envelope_V3_1
    annotation (Placement(transformation(extent={{184,-78},{286,0}})));
  Modelica.Blocks.Sources.RealExpression QGai
    annotation (Placement(transformation(extent={{26,-38},{66,2}})));
  MeasuredQ Qin_sen
    annotation (Placement(transformation(extent={{110,-286},{182,-214}})));
  Modelica.Blocks.Interfaces.RealOutput Q
    annotation (Placement(transformation(extent={{198,-266},{252,-212}})));
  Buildings.Fluid.Sensors.TemperatureTwoPort TR(
    redeclare package Medium = MediumA,
    m_flow_nominal=m_flow_nominal,
    allowFlowReversal=allowFlowReversal) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-168,40})));
  Modelica.Blocks.Sources.RealExpression FromTR(y=TR.T)
    annotation (Placement(transformation(extent={{14,-298},{54,-258}})));
  Modelica.Blocks.Sources.RealExpression FromTS(y=TS.T)
    annotation (Placement(transformation(extent={{38,-274},{78,-234}})));
  Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.VariableSpeed DX(
    redeclare package Medium = MediumA,
    dp_nominal=0,
    datCoi=datCoi,
    minSpeRat=datCoi.minSpeRat,
    T_start=datCoi.sta[1].nomVal.TEvaIn_nominal,
    from_dp=true,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial)
    "Variable speed DX coil"
    annotation (Placement(transformation(extent={{-156,-188},{-136,-168}})));
  Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.DXCoil datCoi(nSta=4,
    minSpeRat=0,
      sta={Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.Stage(
        spe=900/60,
        nomVal=Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.NominalValues(
          Q_flow_nominal=-Qc_nominal/4,
          COP_nominal=3,
          SHR_nominal=0.8,
          m_flow_nominal=m_flow_nominal/4),
        perCur=Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Examples.PerformanceCurves.Curve_I()),Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.Stage(
        spe=1200/60,
        nomVal=Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.NominalValues(
          Q_flow_nominal=-Qc_nominal/3,
          COP_nominal=3,
          SHR_nominal=0.8,
          m_flow_nominal=m_flow_nominal/3),
        perCur=Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Examples.PerformanceCurves.Curve_I()),Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.Stage(
        spe=1800/60,
        nomVal=Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.NominalValues(
          Q_flow_nominal=-Qc_nominal/2,
          COP_nominal=3,
          SHR_nominal=0.8,
          m_flow_nominal=m_flow_nominal/2),
        perCur=Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Examples.PerformanceCurves.Curve_II()),Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.Stage(
        spe=2400/60,
        nomVal=Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.NominalValues(
          Q_flow_nominal=-Qc_nominal/1,
          COP_nominal=3,
          SHR_nominal=0.8,
          m_flow_nominal=m_flow_nominal/1),
        perCur=Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Examples.PerformanceCurves.Curve_III())}) "Coil data"
    annotation (Placement(transformation(extent={{146,110},{166,130}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus1
    annotation (Placement(transformation(extent={{-418,-172},{-398,-152}})));
  BaseClasses.VAVBranch                               cor(
    redeclare package MediumA = MediumA,
    redeclare package MediumW = MediumW,
    m_flow_nominal=mCor_flow_nominal,
    VRoo=VRooCor,
    allowFlowReversal=allowFlowReversal)
    "Zone for core of buildings (azimuth will be neglected)"
    annotation (Placement(transformation(extent={{134,-150},{174,-110}})));
  Modelica.Blocks.Sources.RealExpression yReHea(y=0)
    "reheat coil valve position"
    annotation (Placement(transformation(extent={{84,-148},{104,-128}})));
  Modelica.Blocks.Sources.RealExpression SP_TSA(y=10)
    annotation (Placement(transformation(extent={{-214,-326},{-194,-306}})));
  Buildings.Fluid.Sensors.Pressure sen_PRA(redeclare package Medium = MediumA)
    annotation (Placement(transformation(extent={{-8,40},{-28,60}})));
  Buildings.Fluid.Sensors.Pressure sen_PSA(redeclare package Medium = MediumA)
    annotation (Placement(transformation(extent={{24,-178},{4,-158}})));
  Modelica.Blocks.Sources.RealExpression SP_DP_Pa(y=500)
    annotation (Placement(transformation(extent={{-148,-146},{-108,-106}})));
equation

  connect(clock.y, SC.cur_t) annotation (Line(points={{-47.5,-75},{-43.75,-75},
          {-43.75,-74.2},{-38.2,-74.2}},
                                      color={0,0,127}));
  connect(souInf.ports[1], eco.port_Out) annotation (Line(points={{-376,-20.8},{
          -352,-20.8},{-352,-12},{-328,-12}}, color={0,127,255}));
  connect(souInf.ports[2], mO.port_a) annotation (Line(points={{-376,-23.2},{-368,
          -23.2},{-368,-43},{-364,-43}}, color={0,127,255}));
  connect(mO.port_b, eco.port_Exh) annotation (Line(points={{-342,-43},{-334,-43},
          {-334,-24},{-328,-24}}, color={0,127,255}));
  connect(TS.port_b, mS.port_a)
    annotation (Line(points={{-20,-178},{30,-178}}, color={0,127,255}));
  connect(eco.port_Ret, TM.port_a) annotation (Line(points={{-308,-24},{-294,-24},
          {-294,-178},{-284,-178}}, color={0,127,255}));
  connect(heaCoi.port_a2, TM.port_b)
    annotation (Line(points={{-254,-178},{-264,-178}}, color={0,127,255}));
  connect(souHea.ports[1], heaCoi.port_a1) annotation (Line(points={{-228,-248},
          {-228,-190},{-234,-190}}, color={0,127,255}));
  connect(heaCoi.port_b1, sinHea.ports[1]) annotation (Line(points={{-254,-190},
          {-262,-190},{-262,-238}}, color={0,127,255}));
  connect(fanSup.port_b, TS.port_a)
    annotation (Line(points={{-78,-178},{-40,-178}}, color={0,127,255}));
  connect(realExpression6.y, add.u1) annotation (Line(points={{-369,118},{-348,118},
          {-348,110},{-334,110}},      color={0,0,127}));
  connect(heaCoi.port_b2,TSh. port_a)
    annotation (Line(points={{-234,-178},{-216,-178}}, color={0,127,255}));
  connect(realExpression1.y, souHea.m_flow_in) annotation (Line(points={{-291,
          -288},{-238,-288},{-238,-270},{-236,-270}}, color={0,0,127}));
  connect(realExpression3.y, add2.u2) annotation (Line(points={{-87,-392},{-104,
          -392},{-104,-382},{-118,-382}}, color={0,0,127}));
  connect(TS.T, add2.u1) annotation (Line(points={{-30,-167},{-18,-167},{-18,-370},
          {-118,-370}}, color={0,0,127}));
  connect(SC.Tu, LPF.u[1]) annotation (Line(points={{-31.4,-74.6},{-31.4,-75.3},
          {-10,-75.3},{-10,-76}},             color={0,0,127}));
  connect(partialModel_Envelope_V3_1.weaBus, souInf.weaBus) annotation (Line(
      points={{287.913,-54.6},{340,-54.6},{340,140},{-388,140},{-388,-21.88}},
      color={255,204,51},
      thickness=0.5));
  connect(QGai.y, partialModel_Envelope_V3_1.Q) annotation (Line(points={{68,
          -18},{130,-18},{130,-17.55},{184,-17.55}}, color={0,0,127}));
  connect(Qin_sen.Qo,Q)  annotation (Line(points={{174.8,-239.2},{192.4,-239.2},
          {192.4,-239},{225,-239}}, color={0,0,127}));
  connect(mS.V_flow, Qin_sen.V) annotation (Line(points={{40,-167},{40,-146},{
          68,-146},{68,-222},{119.36,-222},{119.36,-231.28}}, color={0,0,127}));
  connect(partialModel_Envelope_V3_1.b, TR.port_a) annotation (Line(points={{
          286,-0.975},{304,-0.975},{304,40},{-158,40}}, color={0,127,255}));
  connect(TR.port_b, eco.port_Sup) annotation (Line(points={{-178,40},{-298,40},
          {-298,-12},{-308,-12}},                     color={0,127,255}));
  connect(FromTR.y, Qin_sen.To) annotation (Line(points={{56,-278},{104,-278},{104,
          -277.36},{121.52,-277.36}},     color={0,0,127}));
  connect(FromTS.y, Qin_sen.Ti) annotation (Line(points={{80,-254},{102,-254},{
          102,-254.32},{120.08,-254.32}}, color={0,0,127}));
  connect(yOA.y, add.u2) annotation (Line(points={{-366.9,89},{-345.45,89},{-345.45,
          98},{-334,98}}, color={0,0,127}));
  connect(yOA.y, eco.yExh) annotation (Line(points={{-366.9,89},{-358,89},{-358,
          6},{-311,6},{-311,-6}}, color={0,0,127}));
  connect(add.y, eco.yRet) annotation (Line(points={{-311,104},{-296,104},{-296,
          66},{-324.8,66},{-324.8,-6}}, color={0,0,127}));
  connect(yOA.y, eco.yOut) annotation (Line(points={{-366.9,89},{-358,89},{-358,
          6},{-318,6},{-318,-6}}, color={0,0,127}));
  connect(add2.y, cooCoiCon.u_m) annotation (Line(points={{-141,-376},{-162,-376},
          {-162,-350},{-122,-350},{-122,-330}}, color={0,0,127}));
  connect(TSh.port_b, DX.port_a)
    annotation (Line(points={{-196,-178},{-156,-178}}, color={0,127,255}));
  connect(DX.port_b, fanSup.port_a)
    annotation (Line(points={{-136,-178},{-98,-178}}, color={0,127,255}));
  connect(partialModel_Envelope_V3_1.weaBus, weaBus1) annotation (Line(
      points={{287.913,-54.6},{342,-54.6},{342,138},{-408,138},{-408,-162}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{6,3},{6,3}},
      horizontalAlignment=TextAlignment.Left));
  connect(weaBus1.TDryBul, DX.TConIn) annotation (Line(
      points={{-408,-162},{-184,-162},{-184,-175},{-157,-175}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%first",
      index=-1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(cooCoiCon.y, DX.speRat) annotation (Line(points={{-111,-318},{-68,
          -318},{-68,-240},{-168,-240},{-168,-170},{-157,-170}},
                                                           color={0,0,127}));
  connect(cor.port_a, mS.port_b) annotation (Line(points={{144,-150},{146,-150},
          {146,-178},{50,-178}}, color={0,127,255}));
  connect(cor.port_b, partialModel_Envelope_V3_1.a) annotation (Line(points={{
          144,-110},{144,0},{183.044,0}}, color={0,127,255}));
  connect(partialModel_Envelope_V3_1.y, Thermostat.u_m) annotation (Line(points
        ={{289.188,-65},{318,-65},{318,-100},{74,-100},{74,-88}}, color={0,0,
          127}));
  connect(LPF.y[1], Thermostat.u_s) annotation (Line(points={{13,-76},{62,-76}},
                                    color={0,0,127}));
  connect(Thermostat.y, cor.yVAV)
    annotation (Line(points={{85,-76},{104,-76},{104,-122},{130,-122}},
                                                      color={0,0,127}));
  connect(yReHea.y, cor.yVal) annotation (Line(points={{105,-138},{130,-138}},
                             color={0,0,127}));
  connect(SP_TSA.y, cooCoiCon.u_s) annotation (Line(points={{-193,-316},{-160,-316},
          {-160,-318},{-134,-318}}, color={0,0,127}));
  connect(partialModel_Envelope_V3_1.b, sen_PRA.port) annotation (Line(points={
          {286,-0.975},{304,-0.975},{304,40},{-18,40}}, color={0,127,255}));
  connect(TS.port_b, sen_PSA.port)
    annotation (Line(points={{-20,-178},{14,-178}}, color={0,127,255}));
  connect(SP_DP_Pa.y, fanSup.dp_in) annotation (Line(points={{-106,-126},{-88,-126},
          {-88,-166}}, color={0,0,127}));
  annotation (
experiment(Tolerance=1e-06, StopTime=3.1536e+07),
__Dymola_Commands(file="ModelWIthHVACDX.mos"),
                              Documentation(info="<html>
<p>
This model is used for the test case 600FF of the BESTEST validation suite.
Case 600FF is a light-weight building.
The room temperature is free floating.
</p>
</html>", revisions="<html>
<ul>
<li>
October 29, 2016, by Michael Wetter:<br/>
Placed a capacity at the room-facing surface
to reduce the dimension of the nonlinear system of equations,
which generally decreases computing time.<br/>
Removed the pressure drop element which is not needed.<br/>
Linearized the radiative heat transfer, which is the default in
the library, and avoids a large nonlinear system of equations.<br/>
This is for
<a href=\"https://github.com/lbl-srg/modelica-buildings/issues/565\">issue 565</a>.
</li>
<li>
December 22, 2014 by Michael Wetter:<br/>
Removed <code>Modelica.Fluid.System</code>
to address issue
<a href=\"https://github.com/lbl-srg/modelica-buildings/issues/311\">#311</a>.
</li>
<li>
October 9, 2013, by Michael Wetter:<br/>
Implemented soil properties using a record so that <code>TSol</code> and
<code>TLiq</code> are assigned.
This avoids an error when the model is checked in the pedantic mode.
</li>
<li>
July 15, 2012, by Michael Wetter:<br/>
Added reference results.
Changed implementation to make this model the base class
for all BESTEST cases.
Added computation of hourly and annual averaged room air temperature.
<li>
October 6, 2011, by Michael Wetter:<br/>
First implementation.
</li>
</ul>
</html>"),
    Diagram(coordinateSystem(extent={{-440,-400},{360,140}}), graphics={Text(
          extent={{-324,126},{-68,62}},
          lineColor={28,108,200},
          textString="Design Condition
Heating: 4kW (20oC) 
Cooling:8kW(22oC) 
300cfm/ton~0.1m3/s/ton 
--> 0.28 kg/s"),                                                        Text(
          extent={{86,-338},{342,-402}},
          lineColor={238,46,47},
          textString="DX coil data spec had column for the air flow rate.
The minimum compressor speed makes DX coil run during night time making the supply temperature very cold.
what is m_flow_nominal for damper? When DP changes it changes. Therefore m_flow_nominal for the damper is defined on a nominal DP.
Damper is closed, mflow was still there. Eliminated after changing to exponential damper.",
          horizontalAlignment=TextAlignment.Left)}),
    Icon(coordinateSystem(extent={{-440,-400},{360,140}})));
end ModelWithHVACDX_VariablePMV;
