within modelicamodel.BaseClasses;
package Example
  extends Modelica.Icons.ExamplesPackage;
  model Example_VAVbox
    extends Modelica.Icons.Example;
    replaceable package MediumA = Buildings.Media.Air;
    replaceable package MediumW = Buildings.Media.Water;
    parameter Modelica.SIunits.MassFlowRate mCor_flow_nominal=0.1;
    parameter Real VRooCor=1;
    parameter Boolean allowFlowReversal=true;
    output Real Qairflow;

    VAVBranch                                           cor(
      redeclare package MediumA = MediumA,
      redeclare package MediumW = MediumW,
      m_flow_nominal=mCor_flow_nominal,
      VRoo=VRooCor,
      allowFlowReversal=allowFlowReversal)
      "Zone for core of buildings (azimuth will be neglected)"
      annotation (Placement(transformation(extent={{-26,-10},{14,30}})));
    Modelica.Fluid.Sources.FixedBoundary RoomNode(
      redeclare package Medium = MediumA,
      p(displayUnit="Pa") = 0,
      nPorts=1)
      "Fluid connector b (positive design flow direction is from port_a1 to port_b1)"
      annotation (Placement(transformation(extent={{16,32},{36,52}})));
    Modelica.Blocks.Sources.RealExpression yReHea(y=0) "Signal for VAV damper"
      annotation (Placement(transformation(extent={{-84,-18},{-44,22}})));
    Modelica.Blocks.Sources.Ramp yVAV(
      height=-1,                                duration=3600,
      offset=1)
      "Signal for VAV damper"
      annotation (Placement(transformation(extent={{-90,32},{-50,72}})));
    Modelica.Fluid.Sources.FixedBoundary SupplyPressureNode(
      redeclare package Medium = MediumA,
      p(displayUnit="Pa") = 10,
      T=283.15,
      nPorts=1)
      "Fluid connector b (positive design flow direction is from port_a1 to port_b1)"
      annotation (Placement(transformation(extent={{-84,-40},{-64,-20}})));
  equation
    Qairflow=cor.port_a.m_flow*cor.port_b.h_outflow;
    connect(cor.port_b, RoomNode.ports[1]) annotation (Line(points={{-16,30},{-16,
            60},{46,60},{46,42},{36,42}}, color={0,127,255}));
    connect(yReHea.y, cor.yVal)
      annotation (Line(points={{-42,2},{-30,2}}, color={0,0,127}));
    connect(yVAV.y, cor.yVAV) annotation (Line(points={{-48,52},{-48,22},{-34,22},
            {-34,18},{-30,18}}, color={0,0,127}));
    connect(SupplyPressureNode.ports[1], cor.port_a) annotation (Line(points={{
            -64,-30},{-16,-30},{-16,-10}}, color={0,127,255}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
          coordinateSystem(preserveAspectRatio=false), graphics={Text(
            extent={{-46,96},{84,68}},
            lineColor={28,108,200},
            textString="for registance curve (on pump/fan curve), 
I need mflownominal and DP @ fully opened.")}),
      __Dymola_Commands(file="Example_VAVbox.mos"));
  end Example_VAVbox;

  model Example_DX
    extends Modelica.Icons.Example;
    replaceable package MediumA = Buildings.Media.Air;
    parameter Modelica.SIunits.HeatFlowRate Qc_nominal=10000; // 10 kW max

    Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.VariableSpeed DX(
      redeclare package Medium = MediumA,
      show_T=true,
      dp_nominal=0,
      datCoi=datCoi,
      minSpeRat=datCoi.minSpeRat,
      T_start=datCoi.sta[1].nomVal.TEvaIn_nominal,
      from_dp=true,
      energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial)
      "Variable speed DX coil"
      annotation (Placement(transformation(extent={{-8,-8},{12,12}})));
    Modelica.Fluid.Sources.MassFlowSource_T boundary(
      use_m_flow_in=true,
      m_flow=1,
      T=293.15,                                      nPorts=1,redeclare package
        Medium =                                                                         MediumA)
      "Fluid connector a (positive design flow direction is from port_a to port_b)"
      annotation (Placement(transformation(extent={{-66,-8},{-46,12}})));
    Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.DXCoil datCoi(nSta=4,
        sta={
          Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.Stage(
          spe=900/60,
          nomVal=
            Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.NominalValues(
            Q_flow_nominal=-Qc_nominal/4,
            COP_nominal=3,
            SHR_nominal=0.8,
            m_flow_nominal=0.9),
          perCur=
            Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Examples.PerformanceCurves.Curve_I()),
          Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.Stage(
          spe=1200/60,
          nomVal=
            Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.NominalValues(
            Q_flow_nominal=-Qc_nominal/3,
            COP_nominal=3,
            SHR_nominal=0.8,
            m_flow_nominal=1.2),
          perCur=
            Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Examples.PerformanceCurves.Curve_I()),
          Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.Stage(
          spe=1800/60,
          nomVal=
            Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.NominalValues(
            Q_flow_nominal=-Qc_nominal/2,
            COP_nominal=3,
            SHR_nominal=0.8,
            m_flow_nominal=1.5),
          perCur=
            Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Examples.PerformanceCurves.Curve_II()),
          Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.Stage(
          spe=2400/60,
          nomVal=
            Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.NominalValues(
            Q_flow_nominal=-Qc_nominal/1,
            COP_nominal=3,
            SHR_nominal=0.8,
            m_flow_nominal=1.8),
          perCur=
            Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Examples.PerformanceCurves.Curve_III())})      "Coil data"
      annotation (Placement(transformation(extent={{66,72},{86,92}})));
    Modelica.Fluid.Sources.FixedBoundary boundary1(         redeclare package
        Medium =                                                                       MediumA,
        nPorts=1)
      "Fluid connector b (positive design flow direction is from port_a to port_b)"
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={72,2})));
    Modelica.Blocks.Sources.RealExpression TOA(y=273.15 + 20)
      "Outside air dry bulb temperature for an air cooled condenser or wetbulb temperature for an evaporative cooled condenser"
      annotation (Placement(transformation(extent={{-100,16},{-76,34}})));
    Modelica.Blocks.Sources.Ramp speRat1(height=1, duration=3600)
                                                "Speed ratio"
      annotation (Placement(transformation(extent={{-90,58},{-72,76}})));
    Modelica.Blocks.Sources.Ramp speRat2(
      height=1,
      duration=3600,
      offset=1,
      startTime=4000)                           "Speed ratio"
      annotation (Placement(transformation(extent={{-118,2},{-100,20}})));
  equation
    connect(boundary.ports[1], DX.port_a)
      annotation (Line(points={{-46,2},{-8,2}}, color={0,127,255}));
    connect(TOA.y, DX.TConIn) annotation (Line(points={{-74.8,25},{-24,25},{-24,
            5},{-9,5}}, color={0,0,127}));
    connect(speRat1.y, DX.speRat) annotation (Line(points={{-71.1,67},{-18,67},
            {-18,10},{-9,10}}, color={0,0,127}));
    connect(DX.port_b, boundary1.ports[1]) annotation (Line(points={{12,2},{38,
            2},{38,2},{62,2}}, color={0,127,255}));
    connect(speRat2.y, boundary.m_flow_in) annotation (Line(points={{-99.1,11},
            {-79.55,11},{-79.55,10},{-66,10}}, color={0,0,127}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
          coordinateSystem(preserveAspectRatio=false)),
      __Dymola_Commands(file="Example_DX.mos"));
  end Example_DX;

  model Example_PartialModel_Envelopes
    extends Modelica.Icons.Example;
    package MediumA=Buildings.Media.Air;

    parameter Modelica.SIunits.MassFlowRate m_flow_nominal=0.28 "Nominal mass flow rate";
    PartialModel_Envelope partialModel_Envelope
      annotation (Placement(transformation(extent={{-14,-24},{46,22}})));
    Buildings.Fluid.Sources.Boundary_pT bou(          redeclare package Medium=MediumA,
        nPorts=1)
      annotation (Placement(transformation(extent={{28,54},{48,74}})));
    Buildings.Fluid.Sources.MassFlowSource_T boundary(
      use_m_flow_in=false,
      m_flow=m_flow_nominal,
      T=281.15,                                       nPorts=1, redeclare
        package
        Medium =                                                                         MediumA)
      annotation (Placement(transformation(extent={{-62,66},{-42,86}})));
    Modelica.Blocks.Sources.RealExpression pert[2](y={0,0})
      "Prescribed mass flow rate"
      annotation (Placement(transformation(extent={{-132,-12},{-92,28}})));
    Modelica.Blocks.Sources.RealExpression m_flow_in3(y=0)
      "Prescribed mass flow rate"
      annotation (Placement(transformation(extent={{-100,-82},{-60,-42}})));
    Buildings.Fluid.FixedResistances.PressureDrop res(
      redeclare package Medium = MediumA,
      m_flow_nominal=m_flow_nominal,
      dp_nominal=1)
      annotation (Placement(transformation(extent={{60,30},{80,50}})));
  equation
    connect(boundary.ports[1], partialModel_Envelope.a) annotation (Line(points={{-42,76},
            {-30,76},{-30,20.16},{-13.7692,20.16}},         color={0,127,255}));
    connect(m_flow_in3.y, partialModel_Envelope.Q) annotation (Line(points={{
            -58,-62},{-48,-62},{-48,-1},{-14,-1}}, color={0,0,127}));
    connect(pert.y, partialModel_Envelope.Pert) annotation (Line(points={{-90,8},
            {-67,8},{-67,8.2},{-14,8.2}}, color={0,0,127}));
    connect(partialModel_Envelope.b, res.port_a) annotation (Line(points={{46,
            21.31},{52,21.31},{52,40},{60,40}}, color={0,127,255}));
    connect(res.port_b, bou.ports[1]) annotation (Line(points={{80,40},{88,40},
            {88,64},{48,64}}, color={0,127,255}));
    annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
          coordinateSystem(preserveAspectRatio=false)));
  end Example_PartialModel_Envelopes;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Example;
