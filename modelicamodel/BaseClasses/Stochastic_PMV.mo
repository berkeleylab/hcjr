within modelicamodel.BaseClasses;
model Stochastic_PMV "This codes implements the followings. 
  1) for each day, it randomly chooses the number of occupancts and additive PMV disturbances, 
  2) for each day, cacluates the arrival and departure times for each occupant, 
  3) for each person, it outputs PMV according to Fanger's model with the disturbance when he/she is in the zone, otherwise a negative PMV"

    Modelica.Blocks.Interfaces.RealInput TAir_In(unit="K")
    annotation (Placement(transformation(extent={{-120,70},{-80,110}})));

        Modelica.Blocks.Interfaces.RealInput TRad_In(unit="K")
    annotation (Placement(transformation(extent={{-120,-20},{-80,20}})));

        Modelica.Blocks.Interfaces.RealInput phi_In
    annotation (Placement(transformation(extent={{-120,-106},{-80,-66}})));

  parameter Integer MaxNumOcc=5;
  //Real ICl_in[MaxNumOcc]={1.0,1,1.0,1.0};
  //Buildings.Utilities.Comfort.Fanger FangerM[MaxNumOcc](TAir=273.15+20,TRad=273.15+20,phi=0.5);
  Buildings.Utilities.Comfort.Fanger FangerM; // define FangerM (inputs are defined later)

  Real arr_time[MaxNumOcc];
  Real dep_time[MaxNumOcc];
  modelicamodel.BaseClasses.H_NormalNoise RNGenOcc(samplePeriod=24*3600,mu=4,sigma=0.3); // number of occupants
  modelicamodel.BaseClasses.H_NormalNoise RNGen_Personal[MaxNumOcc](samplePeriod=24*3600,mu=0,sigma=1); // additive PMV disturbances
  modelicamodel.BaseClasses.H_NormalNoise RNGenA[MaxNumOcc](samplePeriod=24*3600,mu=8,sigma=0.4); // RN generator for arrival time
  modelicamodel.BaseClasses.H_NormalNoise RNGenD[MaxNumOcc](samplePeriod=24*3600,mu=18,sigma=0.4); // RN generator for departure time

  Real NumOcc(start=3);

  Real dPMV[MaxNumOcc];

    Modelica.Blocks.Interfaces.RealOutput  PMV_env  annotation (Placement(transformation(
          extent={{100,20},{134,54}}),iconTransformation(extent={{100,20},{134,
            54}})));
// PMV calculated from indoor environmental variables

  Modelica.Blocks.Interfaces.RealOutput PMV[MaxNumOcc]  annotation (Placement(transformation(
          extent={{100,-64},{134,-30}}),
                                      iconTransformation(extent={{100,-64},{134,
            -30}})));
  // PMV_env+ personal distrubances
equation
  NumOcc=max(min(floor(RNGenOcc.y),MaxNumOcc),0);

  FangerM.TAir=TAir_In;
  FangerM.TRad=TRad_In;
  FangerM.phi=phi_In;
  FangerM.ICl_in=1;

  PMV_env=FangerM.PMV;

    for i in 1:MaxNumOcc loop
     //calculate the arrival and departure times for each person

     arr_time[i]=RNGenA[i].y; // in R
     dep_time[i]=RNGenD[i].y; // in R
     dPMV[i]=RNGen_Personal[i].y;

     // calculates PMV
     //FangerM[i].ICl_in=ICl_in[i];
     if (arr_time[i]*3600)<=rem(time,24*3600) and rem(time,24*3600)<=(dep_time[i]*3600) then
       //PMV[i]=Buildings.Utilities.Math.Functions.smoothMin(Buildings.Utilities.Math.Functions.smoothMax(FangerM[i].PMV,-3,0.1),3,0.1);
       PMV[i]=Buildings.Utilities.Math.Functions.smoothMin(Buildings.Utilities.Math.Functions.smoothMax(PMV_env+dPMV[i],-3,0.1),3,0.1);

     else
       PMV[i]= -4;
     end if;

  end for;

  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
          Rectangle(
          extent={{-98,100},{98,-100}},
          lineColor={28,108,200},
          fillColor={197,197,197},
          fillPattern=FillPattern.Solid), Text(
          extent={{-78,70},{78,-58}},
          lineColor={28,108,200},
          fillColor={197,197,197},
          fillPattern=FillPattern.None,
          textString="Human")}),                                 Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=604800, __Dymola_Algorithm="Dassl"));
end Stochastic_PMV;
