within modelicamodel.BaseClasses;
model PartialModel_Envelope_V3
  "From Case600FF_io_thickmass_Conv_PMV_multipeople"
  package MediumA = Buildings.Media.Air "Medium model";
  parameter Modelica.SIunits.Angle S_=
    Buildings.Types.Azimuth.S "Azimuth for south walls";
  parameter Modelica.SIunits.Angle E_=
    Buildings.Types.Azimuth.E "Azimuth for east walls";
  parameter Modelica.SIunits.Angle W_=
    Buildings.Types.Azimuth.W "Azimuth for west walls";
  parameter Modelica.SIunits.Angle N_=
    Buildings.Types.Azimuth.N "Azimuth for north walls";
  parameter Modelica.SIunits.Angle C_=
    Buildings.Types.Tilt.Ceiling "Tilt for ceiling";
  parameter Modelica.SIunits.Angle F_=
    Buildings.Types.Tilt.Floor "Tilt for floor";
  parameter Modelica.SIunits.Angle Z_=
    Buildings.Types.Tilt.Wall "Tilt for wall";
  parameter Integer nConExtWin = 1 "Number of constructions with a window";
  parameter Integer nConBou = 1
    "Number of surface that are connected to constructions that are modeled inside the room";
  parameter Buildings.HeatTransfer.Data.OpaqueConstructions.Generic matExtWal(
    nLay=3,
    absIR_a=0.9,
    absIR_b=0.9,
    absSol_a=0.6,
    absSol_b=0.6,
    material={Buildings.HeatTransfer.Data.Solids.Generic(
        x=0.009,
        k=0.140,
        c=900,
        d=530,
        nStaRef=Buildings.ThermalZones.Detailed.Validation.BESTEST.nStaRef),
                         Buildings.HeatTransfer.Data.Solids.Generic(
        x=0.066,
        k=0.040,
        c=840,
        d=12,
        nStaRef=Buildings.ThermalZones.Detailed.Validation.BESTEST.nStaRef),
                         Buildings.HeatTransfer.Data.Solids.Generic(
        x=0.012,
        k=0.160,
        c=840,
        d=950,
        nStaRef=Buildings.ThermalZones.Detailed.Validation.BESTEST.nStaRef)})
                           "Exterior wall"
    annotation (Placement(transformation(extent={{20,84},{34,98}})));
   parameter Buildings.HeatTransfer.Data.Solids.Generic soil(
    x=2,
    k=1.3,
    c=800,
    d=1500) "Soil properties"
    annotation (Placement(transformation(extent={{38,42},{58,62}})));

  Buildings.ThermalZones.Detailed.MixedAir roo(
    redeclare package Medium = MediumA,
    hRoo=2.7,
    nConExtWin=nConExtWin,
    nConBou=1,
    nPorts=5,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial,
    AFlo=48,
    datConBou(
      layers={matFlo},
      each A=48,
      each til=F_),
    datConExt(
      layers={roof,matExtWal,matExtWal,matExtWal},
      A={48,6*2.7,6*2.7,8*2.7},
      til={C_,Z_,Z_,Z_},
      azi={S_,W_,E_,N_}),
    nConExt=4,
    nConPar=0,
    nSurBou=0,
    datConExtWin(
      layers={matExtWal},
      A={8*2.7},
      glaSys={window600},
      wWin={2*3},
      hWin={2},
      fFra={0.001},
      til={Z_},
      azi={S_}),
    lat=weaDat.lat,
    massDynamics=Modelica.Fluid.Types.Dynamics.SteadyState)
    "Room model for Case 600"
    annotation (Placement(transformation(extent={{36,-30},{66,0}})));
  Modelica.Blocks.Sources.Constant qConGai_flow(k=0.5*80/48)
                                                         "Convective heat gain"
    annotation (Placement(transformation(extent={{-56,64},{-48,72}})));
  Modelica.Blocks.Sources.Constant qRadGai_flow(k=0*0.5*120/48)
                                                          "Radiative heat gain"
    annotation (Placement(transformation(extent={{-44,72},{-36,80}})));
  Modelica.Blocks.Routing.Multiplex3 multiplex3_1
    annotation (Placement(transformation(extent={{-18,64},{-10,72}})));
  Modelica.Blocks.Sources.Constant uSha(k=0)
    "Control signal for the shading device"
    annotation (Placement(transformation(extent={{-28,76},{-20,84}})));
  Modelica.Blocks.Routing.Replicator replicator(nout=max(1,nConExtWin))
    annotation (Placement(transformation(extent={{-12,76},{-4,84}})));
  parameter Buildings.HeatTransfer.Data.OpaqueConstructions.Generic roof(nLay=3,
    absIR_a=0.9,
    absIR_b=0.9,
    absSol_a=0.6,
    absSol_b=0.6,
    material={Buildings.HeatTransfer.Data.Solids.Generic(
        x=0.019,
        k=0.140,
        c=900,
        d=530,
        nStaRef=Buildings.ThermalZones.Detailed.Validation.BESTEST.nStaRef),
                         Buildings.HeatTransfer.Data.Solids.Generic(
        x=0.1118,
        k=0.040,
        c=840,
        d=12,
        nStaRef=Buildings.ThermalZones.Detailed.Validation.BESTEST.nStaRef),
                         Buildings.HeatTransfer.Data.Solids.Generic(
        x=0.010,
        k=0.160,
        c=840,
        d=950,
        nStaRef=Buildings.ThermalZones.Detailed.Validation.BESTEST.nStaRef)})
                           "Roof"
    annotation (Placement(transformation(extent={{60,84},{74,98}})));
  Buildings.ThermalZones.Detailed.Validation.BESTEST.Data.Win600
         window600(
    UFra=3,
    haveExteriorShade=false,
    haveInteriorShade=false) "Window"
    annotation (Placement(transformation(extent={{40,84},{54,98}})));
  Buildings.Fluid.Sources.MassFlowSource_T sinInf(
    redeclare package Medium = MediumA,
    m_flow=1,
    use_m_flow_in=true,
    use_T_in=false,
    use_X_in=false,
    use_C_in=false,
    nPorts=1) "Sink model for air infiltration"
    annotation (Placement(transformation(extent={{4,-66},{16,-54}})));
  Buildings.Fluid.Sources.Outside souInf(redeclare package Medium = MediumA,
      nPorts=1) "Source model for air infiltration"
           annotation (Placement(transformation(extent={{-24,-34},{-12,-22}})));
  Modelica.Blocks.Sources.Constant InfiltrationRate(k=-48*2.7*0.5/3600*0)
    "0.41 ACH adjusted for the altitude (0.5 at sea level)"
    annotation (Placement(transformation(extent={{-96,-78},{-88,-70}})));
  Modelica.Blocks.Math.Product product
    annotation (Placement(transformation(extent={{-50,-60},{-40,-50}})));
  Buildings.Fluid.Sensors.Density density(redeclare package Medium = MediumA)
    "Air density inside the building"
    annotation (Placement(transformation(extent={{-40,-76},{-50,-66}})));
  Modelica.Thermal.HeatTransfer.Sensors.TemperatureSensor TRooAir
    "Room air temperature"
    annotation (Placement(transformation(extent={{-86,-28},{-78,-20}})));
  replaceable parameter
    Buildings.ThermalZones.Detailed.Validation.BESTEST.Data.StandardResultsFreeFloating
      staRes(
        minT( Min=-18.8+273.15, Max=-15.6+273.15, Mean=-17.6+273.15),
        maxT( Min=64.9+273.15,  Max=69.5+273.15,  Mean=66.2+273.15),
        meanT(Min=24.2+273.15,  Max=25.9+273.15,  Mean=25.1+273.15))
          constrainedby Modelica.Icons.Record
    "Reference results from ASHRAE/ANSI Standard 140"
    annotation (Placement(transformation(extent={{80,40},{94,54}})));
  Modelica.Blocks.Math.MultiSum multiSum(nu=1)
    annotation (Placement(transformation(extent={{-78,-80},{-66,-68}})));
  Modelica.Blocks.Math.Mean TRooHou(f=1/3600, y(start=293.15))
    "Hourly averaged room air temperature"
    annotation (Placement(transformation(extent={{-68,-28},{-60,-20}})));
  Modelica.Blocks.Math.Mean TRooAnn(f=1/86400/365, y(start=293.15))
    "Annual averaged room air temperature"
    annotation (Placement(transformation(extent={{-68,-40},{-60,-32}})));

  Modelica.Blocks.Interfaces.RealOutput y
    annotation (Placement(transformation(extent={{10,-10},{-10,10}},
        rotation=180,
        origin={110,-100})));
  parameter Buildings.HeatTransfer.Data.OpaqueConstructions.Generic matFlo(
    nLay=1,
    absIR_a=0.9,
    absIR_b=0.9,
    absSol_a=0.6,
    absSol_b=0.6,
    material={Buildings.HeatTransfer.Data.Solids.Generic(
        x=0.025,
        k=0.140,
        c=1200,
        d=650,
        nStaRef=Buildings.ThermalZones.Detailed.Validation.BESTEST.nStaRef)})
    "Floor" annotation (Placement(transformation(extent={{80,84},{94,98}})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow preHea1
    "Prescribed heat flow for heating and cooling"
    annotation (Placement(transformation(extent={{46,-64},{58,-52}})));
  Modelica.Blocks.Sources.Constant const1(k=0)
    annotation (Placement(transformation(extent={{38,-82},{46,-74}})));
  Stochastic_PMV                     Comfort
    annotation (Placement(transformation(extent={{-144,-42},{-174,-16}})));
  Buildings.Fluid.Sensors.RelativeHumidity senRelHum(redeclare package Medium
      = MediumA)
    annotation (Placement(transformation(extent={{-84,-40},{-98,-28}})));
  Modelica.Blocks.Interfaces.RealOutput PMV[5] "True_Comfort" annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={110,-52})));
  Modelica.Fluid.Interfaces.FluidPort_a a(redeclare package Medium = MediumA)
    "Fluid inlets and outlets" annotation (Placement(transformation(
        extent={{-24,-19},{24,19}},
        rotation=90,
        origin={-223,100})));
  Modelica.Fluid.Interfaces.FluidPort_b b(redeclare package Medium = MediumA)
    "Fluid inlets and outlets" annotation (Placement(transformation(
        extent={{-19,-20},{19,20}},
        rotation=270,
        origin={100,97})));
  Buildings.BoundaryConditions.WeatherData.ReaderTMY3 weaDat(filNam=
        Modelica.Utilities.Files.loadResource("modelica://Buildings/Resources/weatherdata/DRYCOLD.mos"),
      computeWetBulbTemperature=false)
    annotation (Placement(transformation(extent={{100,-94},{88,-82}})));
  Buildings.BoundaryConditions.WeatherData.Bus weaBus
    annotation (Placement(transformation(extent={{98,-76},{114,-60}})));
  Modelica.Thermal.HeatTransfer.Sources.PrescribedHeatFlow preHea
    "Prescribed heat flow for heating and cooling"
    annotation (Placement(transformation(extent={{-80,-6},{-68,6}})));
  Modelica.Blocks.Interfaces.RealInput Q
    annotation (Placement(transformation(extent={{-240,26},{-200,66}})));
  Modelica.Blocks.Sources.Constant qLatGai_flow(k=0) "Latent heat gain"
    annotation (Placement(transformation(extent={{-44,56},{-36,64}})));
  Modelica.Blocks.Interfaces.RealOutput PMV_env "Fanger_PMV" annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=0,
        origin={110,-28})));
equation
  connect(qRadGai_flow.y,multiplex3_1. u1[1])  annotation (Line(
      points={{-35.6,76},{-34,76},{-34,70.8},{-18.8,70.8}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(multiplex3_1.y, roo.qGai_flow) annotation (Line(
      points={{-9.6,68},{20,68},{20,-9},{34.8,-9}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(roo.uSha, replicator.y) annotation (Line(
      points={{34.8,-1.5},{24,-1.5},{24,80},{-3.6,80}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(qConGai_flow.y, multiplex3_1.u2[1]) annotation (Line(
      points={{-47.6,68},{-18.8,68}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(uSha.y, replicator.u) annotation (Line(
      points={{-19.6,80},{-12.8,80}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(product.y, sinInf.m_flow_in)       annotation (Line(
      points={{-39.5,-55},{-36,-55},{-36,-55.2},{2.8,-55.2}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(density.port, roo.ports[1])  annotation (Line(
      points={{-45,-76},{32,-76},{32,-24.9},{39.75,-24.9}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(density.d, product.u2) annotation (Line(
      points={{-50.5,-71},{-56,-71},{-56,-58},{-51,-58}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(roo.heaPorAir, TRooAir.port)  annotation (Line(
      points={{50.25,-15},{-90,-15},{-90,-24},{-86,-24}},
      color={191,0,0},
      smooth=Smooth.None));
  connect(sinInf.ports[1], roo.ports[2])        annotation (Line(
      points={{16,-60},{30,-60},{30,-23.7},{39.75,-23.7}},
      color={0,127,255},
      smooth=Smooth.None));
  connect(multiSum.y, product.u1) annotation (Line(
      points={{-64.98,-74},{-54,-74},{-54,-52},{-51,-52}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(InfiltrationRate.y, multiSum.u[1]) annotation (Line(
      points={{-87.6,-74},{-78,-74}},
      color={0,0,127},
      smooth=Smooth.None));

  connect(TRooAir.T, TRooHou.u) annotation (Line(
      points={{-78,-24},{-68.8,-24}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TRooAir.T, TRooAnn.u) annotation (Line(
      points={{-78,-24},{-76,-24},{-76,-36},{-68.8,-36}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(souInf.ports[1], roo.ports[3]) annotation (Line(points={{-12,-28},{14,
          -28},{14,-22.5},{39.75,-22.5}}, color={0,127,255}));
  connect(TRooAir.T, y) annotation (Line(points={{-78,-24},{-72,-24},{-72,-100},
          {110,-100}},
                   color={0,0,127}));
  connect(preHea1.port, roo.surf_conBou[1]) annotation (Line(points={{58,-58},{
          60,-58},{60,-27},{55.5,-27}}, color={191,0,0}));
  connect(const1.y, preHea1.Q_flow)
    annotation (Line(points={{46.4,-78},{46,-78},{46,-58}}, color={0,0,127}));
  connect(density.port, senRelHum.port) annotation (Line(points={{-45,-76},{-44,
          -76},{-44,-88},{-104,-88},{-104,-44},{-91,-44},{-91,-40}}, color={0,127,
          255}));
  connect(roo.ports[4], a) annotation (Line(points={{39.75,-21.3},{-50,-21.3},{
          -50,100},{-223,100}},
                          color={0,127,255}));
  connect(roo.ports[5], b) annotation (Line(points={{39.75,-20.1},{100,-20.1},{100,
          97}}, color={0,127,255}));
  connect(weaDat.weaBus, roo.weaBus) annotation (Line(
      points={{88,-88},{78,-88},{78,-1.575},{64.425,-1.575}},
      color={255,204,51},
      thickness=0.5));
  connect(weaDat.weaBus, weaBus) annotation (Line(
      points={{88,-88},{78,-88},{78,-68},{106,-68}},
      color={255,204,51},
      thickness=0.5), Text(
      string="%second",
      index=1,
      extent={{-6,3},{-6,3}},
      horizontalAlignment=TextAlignment.Right));
  connect(weaDat.weaBus, souInf.weaBus) annotation (Line(
      points={{88,-88},{-36,-88},{-36,-27.88},{-24,-27.88}},
      color={255,204,51},
      thickness=0.5));
  connect(preHea.Q_flow, Q)
    annotation (Line(points={{-80,0},{-120,0},{-120,46},{-220,46}},
                                                color={0,0,127}));
  connect(preHea.port, TRooAir.port) annotation (Line(points={{-68,0},{10,0},{
          10,-15},{-90,-15},{-90,-24},{-86,-24}}, color={191,0,0}));
  connect(qLatGai_flow.y, multiplex3_1.u3[1]) annotation (Line(points={{-35.6,
          60},{-28,60},{-28,65.2},{-18.8,65.2}}, color={0,0,127}));
  connect(TRooAir.T, Comfort.TAir_In) annotation (Line(points={{-78,-24},{-74,
          -24},{-74,-17.3},{-144,-17.3}}, color={0,0,127}));
  connect(TRooAir.T, Comfort.TRad_In) annotation (Line(points={{-78,-24},{-78,
          -29},{-144,-29}}, color={0,0,127}));
  connect(senRelHum.phi, Comfort.phi_In) annotation (Line(points={{-98.7,-34},{
          -128,-34},{-128,-40.18},{-144,-40.18}}, color={0,0,127}));
  connect(Comfort.PMV_env, PMV_env) annotation (Line(points={{-176.55,-24.19},{
          -190,-24.19},{-190,-114},{68,-114},{68,-28},{110,-28}}, color={0,0,
          127}));
  connect(Comfort.PMV, PMV) annotation (Line(points={{-176.55,-35.11},{-184,
          -35.11},{-184,-116},{84,-116},{84,-52},{110,-52}}, color={0,0,127}));
  annotation (
experiment(Tolerance=1e-06, StopTime=3.1536e+07),
__Dymola_Commands(file="modelica://Buildings/Resources/Scripts/Dymola/ThermalZones/Detailed/Validation/BESTEST/Cases6xx/Case600FF.mos"
        "Simulate and plot",
      executeCall=simulateModel(
          "modelicamodel.Case600FF_io_thickmass_Conv_PMV_multipeople",
          startTime=15552000,
          stopTime=15638400,
          method="dassl",
          tolerance=1e-06,
          resultFile="Case600FF_io_thickmass_Conv_PMV_multipeople"),
      file="Case600FF_io_thickmass_Conv_PMV_multipeople.mos",
      file="Case600FF_io_thickmass_Conv_PMV_multipeople.mos"),
                              Documentation(info="<html>
<p>
This model is used for the test case 600FF of the BESTEST validation suite.
Case 600FF is a light-weight building.
The room temperature is free floating.
</p>
</html>", revisions="<html>
<ul>
<li>
October 29, 2016, by Michael Wetter:<br/>
Placed a capacity at the room-facing surface
to reduce the dimension of the nonlinear system of equations,
which generally decreases computing time.<br/>
Removed the pressure drop element which is not needed.<br/>
Linearized the radiative heat transfer, which is the default in
the library, and avoids a large nonlinear system of equations.<br/>
This is for
<a href=\"https://github.com/lbl-srg/modelica-buildings/issues/565\">issue 565</a>.
</li>
<li>
December 22, 2014 by Michael Wetter:<br/>
Removed <code>Modelica.Fluid.System</code>
to address issue
<a href=\"https://github.com/lbl-srg/modelica-buildings/issues/311\">#311</a>.
</li>
<li>
October 9, 2013, by Michael Wetter:<br/>
Implemented soil properties using a record so that <code>TSol</code> and
<code>TLiq</code> are assigned.
This avoids an error when the model is checked in the pedantic mode.
</li>
<li>
July 15, 2012, by Michael Wetter:<br/>
Added reference results.
Changed implementation to make this model the base class
for all BESTEST cases.
Added computation of hourly and annual averaged room air temperature.
<li>
October 6, 2011, by Michael Wetter:<br/>
First implementation.
</li>
</ul>
</html>"),
    Diagram(coordinateSystem(extent={{-220,-140},{100,100}})),
    Icon(coordinateSystem(extent={{-220,-140},{100,100}}), graphics={Rectangle(
          extent={{-160,0},{100,-100}},
          lineColor={28,108,200},
          fillColor={215,215,215},
          fillPattern=FillPattern.Solid), Polygon(
          points={{-158,0},{-36,98},{100,0},{-158,0}},
          lineColor={28,108,200},
          fillColor={175,175,175},
          fillPattern=FillPattern.Solid)}));
end PartialModel_Envelope_V3;
