within modelicamodel.BaseClasses.Example;
model Example_DX
  extends Modelica.Icons.Example;
  replaceable package MediumA = Buildings.Media.Air;
  parameter Modelica.SIunits.HeatFlowRate Qc_nominal=10000; // 10 kW max

  Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.VariableSpeed DX(
    redeclare package Medium = MediumA,
    show_T=true,
    dp_nominal=0,
    datCoi=datCoi,
    minSpeRat=datCoi.minSpeRat,
    T_start=datCoi.sta[1].nomVal.TEvaIn_nominal,
    from_dp=true,
    energyDynamics=Modelica.Fluid.Types.Dynamics.FixedInitial)
    "Variable speed DX coil"
    annotation (Placement(transformation(extent={{-8,-8},{12,12}})));
  Modelica.Fluid.Sources.MassFlowSource_T boundary(
    use_m_flow_in=true,
    m_flow=1,
    T=293.15,                                      nPorts=1,redeclare package
      Medium =                                                                         MediumA)
    "Fluid connector a (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(extent={{-66,-8},{-46,12}})));
  Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.DXCoil datCoi(nSta=4,
      sta={
        Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.Stage(
        spe=900/60,
        nomVal=
          Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.NominalValues(
          Q_flow_nominal=-Qc_nominal/4,
          COP_nominal=3,
          SHR_nominal=0.8,
          m_flow_nominal=0.9),
        perCur=
          Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Examples.PerformanceCurves.Curve_I()),
        Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.Stage(
        spe=1200/60,
        nomVal=
          Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.NominalValues(
          Q_flow_nominal=-Qc_nominal/3,
          COP_nominal=3,
          SHR_nominal=0.8,
          m_flow_nominal=1.2),
        perCur=
          Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Examples.PerformanceCurves.Curve_I()),
        Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.Stage(
        spe=1800/60,
        nomVal=
          Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.NominalValues(
          Q_flow_nominal=-Qc_nominal/2,
          COP_nominal=3,
          SHR_nominal=0.8,
          m_flow_nominal=1.5),
        perCur=
          Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Examples.PerformanceCurves.Curve_II()),
        Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.Stage(
        spe=2400/60,
        nomVal=
          Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Data.Generic.BaseClasses.NominalValues(
          Q_flow_nominal=-Qc_nominal/1,
          COP_nominal=3,
          SHR_nominal=0.8,
          m_flow_nominal=1.8),
        perCur=
          Buildings.Fluid.HeatExchangers.DXCoils.AirCooled.Examples.PerformanceCurves.Curve_III())})      "Coil data"
    annotation (Placement(transformation(extent={{66,72},{86,92}})));
  Modelica.Fluid.Sources.FixedBoundary boundary1(         redeclare package
      Medium =                                                                       MediumA,
      nPorts=1)
    "Fluid connector b (positive design flow direction is from port_a to port_b)"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={72,2})));
  Modelica.Blocks.Sources.RealExpression TOA(y=273.15 + 20)
    "Outside air dry bulb temperature for an air cooled condenser or wetbulb temperature for an evaporative cooled condenser"
    annotation (Placement(transformation(extent={{-100,16},{-76,34}})));
  Modelica.Blocks.Sources.Ramp speRat1(height=1, duration=3600)
                                              "Speed ratio"
    annotation (Placement(transformation(extent={{-90,58},{-72,76}})));
  Modelica.Blocks.Sources.Ramp speRat2(
    height=1,
    duration=3600,
    offset=1,
    startTime=4000)                           "Speed ratio"
    annotation (Placement(transformation(extent={{-118,2},{-100,20}})));
equation
  connect(boundary.ports[1], DX.port_a)
    annotation (Line(points={{-46,2},{-8,2}}, color={0,127,255}));
  connect(TOA.y, DX.TConIn) annotation (Line(points={{-74.8,25},{-24,25},{-24,
          5},{-9,5}}, color={0,0,127}));
  connect(speRat1.y, DX.speRat) annotation (Line(points={{-71.1,67},{-18,67},
          {-18,10},{-9,10}}, color={0,0,127}));
  connect(DX.port_b, boundary1.ports[1]) annotation (Line(points={{12,2},{38,
          2},{38,2},{62,2}}, color={0,127,255}));
  connect(speRat2.y, boundary.m_flow_in) annotation (Line(points={{-99.1,11},
          {-79.55,11},{-79.55,10},{-66,10}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    __Dymola_Commands(file="Example_DX.mos"));
end Example_DX;
