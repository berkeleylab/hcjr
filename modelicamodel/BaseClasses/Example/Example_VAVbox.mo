within modelicamodel.BaseClasses.Example;
model Example_VAVbox
  extends Modelica.Icons.Example;
  replaceable package MediumA = Buildings.Media.Air;
  replaceable package MediumW = Buildings.Media.Water;
  parameter Modelica.SIunits.MassFlowRate mCor_flow_nominal=0.1;
  parameter Real VRooCor=1;
  parameter Boolean allowFlowReversal=true;
  output Real Qairflow;

  VAVBranch                                           cor(
    redeclare package MediumA = MediumA,
    redeclare package MediumW = MediumW,
    m_flow_nominal=mCor_flow_nominal,
    VRoo=VRooCor,
    allowFlowReversal=allowFlowReversal)
    "Zone for core of buildings (azimuth will be neglected)"
    annotation (Placement(transformation(extent={{-26,-10},{14,30}})));
  Modelica.Fluid.Sources.FixedBoundary RoomNode(
    redeclare package Medium = MediumA,
    p(displayUnit="Pa") = 0,
    nPorts=1)
    "Fluid connector b (positive design flow direction is from port_a1 to port_b1)"
    annotation (Placement(transformation(extent={{16,32},{36,52}})));
  Modelica.Blocks.Sources.RealExpression yReHea(y=0) "Signal for VAV damper"
    annotation (Placement(transformation(extent={{-84,-18},{-44,22}})));
  Modelica.Blocks.Sources.Ramp yVAV(
    height=-1,                                duration=3600,
    offset=1)
    "Signal for VAV damper"
    annotation (Placement(transformation(extent={{-90,32},{-50,72}})));
  Modelica.Fluid.Sources.FixedBoundary SupplyPressureNode(
    redeclare package Medium = MediumA,
    p(displayUnit="Pa") = 10,
    T=283.15,
    nPorts=1)
    "Fluid connector b (positive design flow direction is from port_a1 to port_b1)"
    annotation (Placement(transformation(extent={{-84,-40},{-64,-20}})));
equation
  Qairflow=cor.port_a.m_flow*cor.port_b.h_outflow;
  connect(cor.port_b, RoomNode.ports[1]) annotation (Line(points={{-16,30},{-16,
          60},{46,60},{46,42},{36,42}}, color={0,127,255}));
  connect(yReHea.y, cor.yVal)
    annotation (Line(points={{-42,2},{-30,2}}, color={0,0,127}));
  connect(yVAV.y, cor.yVAV) annotation (Line(points={{-48,52},{-48,22},{-34,22},
          {-34,18},{-30,18}}, color={0,0,127}));
  connect(SupplyPressureNode.ports[1], cor.port_a) annotation (Line(points={{
          -64,-30},{-16,-30},{-16,-10}}, color={0,127,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false), graphics={Text(
          extent={{-46,96},{84,68}},
          lineColor={28,108,200},
          textString="for registance curve (on pump/fan curve), 
I need mflownominal and DP @ fully opened.")}),
    __Dymola_Commands(file="Example_VAVbox.mos"));
end Example_VAVbox;
