within modelicamodel.BaseClasses.Example;
model Example_Variable_PMV
  " This codes implements the followings. 
  1) for each day, it randomly chooses the number of occupancts and additive PMV disturbances, 
  2) for each day, cacluates the arrival and departure times for each occupant, 
  3) for each person, it outputs PMV according to Fanger's model with the disturbance when he/she is in the zone, otherwise a negative PMV"
  extends Modelica.Icons.Example;


  Stochastic_PMV stochastic_PMV
    annotation (Placement(transformation(extent={{-4,-30},{48,22}})));
  Modelica.Blocks.Sources.RealExpression TAir_In1(y=273.15 + 20)
    annotation (Placement(transformation(extent={{-98,0},{-58,40}})));
  Modelica.Blocks.Sources.RealExpression TRad(y=273.15 + 20)
    annotation (Placement(transformation(extent={{-100,-24},{-60,16}})));
  Modelica.Blocks.Sources.RealExpression phi(y=0.5)
    annotation (Placement(transformation(extent={{-104,-58},{-64,-18}})));
equation
  connect(TAir_In1.y, stochastic_PMV.TAir_In) annotation (Line(points={{-56,20},
          {-30,20},{-30,19.4},{-4,19.4}}, color={0,0,127}));
  connect(TRad.y, stochastic_PMV.TRad_In)
    annotation (Line(points={{-58,-4},{-4,-4}}, color={0,0,127}));
  connect(phi.y, stochastic_PMV.phi_In) annotation (Line(points={{-62,-38},{-33,
          -38},{-33,-26.36},{-4,-26.36}}, color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(StopTime=604800, __Dymola_Algorithm="Dassl"));
end Example_Variable_PMV;
