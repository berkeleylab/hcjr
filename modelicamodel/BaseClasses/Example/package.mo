within modelicamodel.BaseClasses;
package Example
  extends Modelica.Icons.ExamplesPackage;

  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Example;
