within modelicamodel.BaseClasses.Example;
model Example_PartialModel_Envelopes
  extends Modelica.Icons.Example;
  package MediumA=Buildings.Media.Air;

  parameter Modelica.SIunits.MassFlowRate m_flow_nominal=0.28 "Nominal mass flow rate";
  PartialModel_Envelope_V2
                        partialModel_Envelope_V2_1
    annotation (Placement(transformation(extent={{-14,-24},{46,22}})));
  Modelica.Blocks.Sources.RealExpression NumOcc(y=1) "Number of Occupant"
    annotation (Placement(transformation(extent={{-100,-12},{-60,28}})));
  Modelica.Blocks.Sources.RealExpression m_flow_in3(y=0)
    "Prescribed mass flow rate"
    annotation (Placement(transformation(extent={{-100,-82},{-60,-42}})));
equation
  connect(m_flow_in3.y, partialModel_Envelope_V2_1.Q) annotation (Line(points={
          {-58,-62},{-48,-62},{-48,11.65},{-14,11.65}}, color={0,0,127}));
  connect(NumOcc.y, partialModel_Envelope_V2_1.Pert) annotation (Line(points={{-58,8},
          {-54,8},{-54,16.6333},{-14,16.6333}},        color={0,0,127}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Example_PartialModel_Envelopes;
