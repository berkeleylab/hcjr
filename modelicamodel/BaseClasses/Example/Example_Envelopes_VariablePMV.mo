within modelicamodel.BaseClasses.Example;
model Example_Envelopes_VariablePMV
  extends Modelica.Icons.Example;

  PartialModel_Envelope_V2 partialModel_Envelope_V2_1
    annotation (Placement(transformation(extent={{-86,-46},{-4,16}})));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end Example_Envelopes_VariablePMV;
