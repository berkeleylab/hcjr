within modelicamodel;
model H_perturb_Fanger "Test of Comfort Model"
  extends Modelica.Icons.Example;

  Buildings.Utilities.Comfort.Fanger theCom(
    use_vAir_in=true,
    use_M_in=true,
    use_pAir_in=true) "Thermal comfort model"
                                            annotation (
      Placement(transformation(extent={{70,-40},{90,-20}})));
  Modelica.Blocks.Sources.Constant ICl(k=0.9) "Clothing insulation"
    annotation (Placement(transformation(extent=[-14,-90; 6,-70])));
  Modelica.Blocks.Sources.Constant vAir(k=0.05) "Air velocity"
    annotation (Placement(transformation(extent=[-80,-56; -60,-36])));
  Modelica.Blocks.Sources.Constant M(k=60) "Metabolic heat generated"
    annotation (Placement(transformation(extent=[-80,-86; -60,-66])));
  Modelica.Blocks.Interfaces.RealInput
                               TAir
                        "Air temperature"
                        annotation (Placement(
        transformation(extent={{-88,50},{-68,70}})));
  Modelica.Blocks.Interfaces.RealInput
                                   TRad                "Radiation temperature"
    annotation (Placement(transformation(extent={{-88,0},{-68,20}})));
  Modelica.Blocks.Sources.Constant pAtm(k=101325)
    annotation (Placement(transformation(extent=[0,68; 20,88])));
  Modelica.Blocks.Interfaces.RealInput
                                   phi        "Relative humidity"
    annotation (Placement(transformation(extent={{-28,34},{-8,54}})));
  Buildings.Utilities.Comfort.Fanger theComFixPar(
    use_vAir_in=false,
    use_M_in=false,
    use_ICl_in=false,
    use_pAir_in=false,
    ICl=0.9) "Thermal comfort model with fixed parameters"
                                            annotation (
      Placement(transformation(extent={{70,-6},{90,14}})));
equation
  connect(pAtm.y, theCom.pAir_in) annotation (Line(
      points={{21,78},{30,78},{30,-40},{69,-40}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(theCom.vAir_in, vAir.y) annotation (Line(
      points={{69,-31},{-28,-31},{-28,-46},{-59,-46}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(M.y, theCom.M_in) annotation (Line(
      points={{-59,-76},{-20,-76},{-20,-34},{69,-34}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(ICl.y, theCom.ICl_in) annotation (Line(
      points={{7,-80},{20,-80},{20,-37},{69,-37}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(TAir, theComFixPar.TAir) annotation (Line(points={{-78,60},{-46,60},{
          -46,14},{69,14}}, color={0,0,127}));
  connect(TAir, theCom.TAir) annotation (Line(points={{-78,60},{-46,60},{-46,
          -20},{69,-20}}, color={0,0,127}));
  connect(phi, theComFixPar.phi) annotation (Line(points={{-18,44},{52,44},{52,
          6},{69,6}}, color={0,0,127}));
  connect(phi, theCom.phi) annotation (Line(points={{-18,44},{54,44},{54,-28},{
          69,-28}}, color={0,0,127}));
  connect(TRad, theComFixPar.TRad)
    annotation (Line(points={{-78,10},{69,10}}, color={0,0,127}));
  connect(TRad, theCom.TRad) annotation (Line(points={{-78,10},{58,10},{58,-24},
          {69,-24}}, color={0,0,127}));
 annotation (experiment(Tolerance=1e-6, StopTime=1.0),
__Dymola_Commands(file="modelica://Buildings/Resources/Scripts/Dymola/Utilities/Comfort/Examples/Fanger.mos"
        "Simulate and plot"),
    Documentation(info="<html>
This is a test of the Thermal Comfort Model.
</html>"));
end H_perturb_Fanger;
