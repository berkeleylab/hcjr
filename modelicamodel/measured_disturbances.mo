within modelicamodel;
function measured_disturbances
  input Real cur_t;
  output
    outlist     Y;
  parameter   Real HRs[:] =    { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24};//
  parameter   Real UBs[:] =    { 4, 4, 4, 4, 4, 4, 4, 4, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 4, 4, 4, 4, 4, 4};// # temperature upper bound based on 72F see below
  parameter   Real LBs[:] =    {-4,-4,-4,-4,-4,-4,-4,-4,-3,-2,-2,-2,-2,-2,-2,-2,-2,-2,-3,-4,-4,-4,-4,-4,-4};// # temperature lower bound based on 72F
  parameter Real Rs[:] =     { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};// # last term is to prevent extrapolation
protected
  Real hr(min=0, max= 24);

algorithm
  hr  :=   rem(cur_t,3600*24)/3600;
  Y.Tl :=
    Modelica.Math.Vectors.interpolate(
    x=HRs,
    y=LBs,
    xi=hr) + 22 + 273.15;
  Y.Tu :=
    Modelica.Math.Vectors.interpolate(
    x=HRs,
    y=UBs,
    xi=hr) + 22 + 273.15;                            //Modelica.Math.Vectors.interpolate(x=HRs,y=UBs,xi=hr);
  Y.R :=
    Modelica.Math.Vectors.interpolate(
    x=HRs,
    y=Rs,
    xi=hr);
end measured_disturbances;
