within modelicamodel;
block MeasuredQ
  parameter Real Cp= 1000 "Cp of air [J/oc-kg]";
  parameter Real rhoa = 1.2;
  Modelica.Blocks.Interfaces.RealInput V
    annotation (Placement(transformation(extent={{-94,32},{-54,72}})));
  Modelica.Blocks.Interfaces.RealInput Ti
    annotation (Placement(transformation(extent={{-92,-32},{-52,8}})));
  Modelica.Blocks.Interfaces.RealInput To
    annotation (Placement(transformation(extent={{-88,-96},{-48,-56}})));
  Modelica.Blocks.Interfaces.RealOutput Qo
    annotation (Placement(transformation(extent={{58,8},{102,52}})));
  Real dT;
  Modelica.Blocks.Interfaces.RealOutput Qi
    annotation (Placement(transformation(extent={{60,-44},{104,0}})));
equation
  dT=Ti-To;
  Qo=rhoa*V*Cp*dT;
  Qi=-Qo;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false), graphics={
        Rectangle(
          lineColor={200,200,200},
          fillColor={248,248,248},
          fillPattern=FillPattern.HorizontalCylinder,
          extent={{-100.0,-100.0},{100.0,100.0}},
          radius=25.0),
        Rectangle(
          lineColor={128,128,128},
          extent={{-100.0,-100.0},{100.0,100.0}},
          radius=25.0),
        Ellipse(origin={0.0,-30.0},
          fillColor={255,255,255},
          extent={{-90.0,-90.0},{90.0,90.0}},
          startAngle=20.0,
          endAngle=160.0),
        Ellipse(origin={0.0,-30.0},
          fillColor={128,128,128},
          pattern=LinePattern.None,
          fillPattern=FillPattern.Solid,
          extent={{-20.0,-20.0},{20.0,20.0}}),
        Line(origin={0.0,-30.0},
          points={{0.0,60.0},{0.0,90.0}}),
        Ellipse(origin={-0.0,-30.0},
          fillColor={64,64,64},
          pattern=LinePattern.None,
          fillPattern=FillPattern.Solid,
          extent={{-10.0,-10.0},{10.0,10.0}}),
        Polygon(
          origin={-0.0,-30.0},
          rotation=-35.0,
          fillColor={64,64,64},
          pattern=LinePattern.None,
          fillPattern=FillPattern.Solid,
          points={{-7.0,0.0},{-3.0,85.0},{0.0,90.0},{3.0,85.0},{7.0,0.0}})}),
                                                                 Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end MeasuredQ;
