# Model Predictive Controller for HCJR
--------------------------------------------------------------------------------

This is the repository for the model predictive controller (MPC) developed from the HCJR (Hot, Cold or Just Right) project which aims at improving individuals' thermal comfort and minimizing energy consumptions for commercial buildings via developing a new sensing and control technology. 
The new approach is able to continuously monitor individuals' thermal comfort and direclty regulate the comfort via the use of a Thermal Infrared Camera (TIR). The control algorithm is based on Model Predictive Control (MPC) which is shared in this repository. 
More detailed descriptions for overall flow of the proposed approach, the mathematical formulation of the optimal control problem, and case studies are descirbed in the document (./doc/).
This module was developed with Python2.7 but is compatible with Python 3.x. 

# Setup
The MPC-library relies on the following external python libraries:

* PyFMI (2.5 recommended) to run a detailed building simulation model
* CVXPY 1.0 as a wrapper to an optimization solver

# Library description
This MPC library is a free open-source. The library contains the followings.

  
1. hcrj/MPC/doc: documentation 
2. hcrj/MPC/*.ipynb: demo files
3. hcrj/MPC/src: python codes of the MPC-Comfort
4. hcrj/MPC/data

  - fmu models
  - mat files containing parameters of building envelope and fitted PMV models
  - weather data used for the MPC obtained from the fmu simulator   

# Example
Once the external python libraries are installed, one can run the following examples with the Jupyter-notebook.

- ```Demo_MPC_Comfort_disturbance_rejection.ipynb``` 

- ```Demo_MPC_Comfort_Max_multipeople.ipynb``` 

-  ```Demo_MPC_Comfort_Mean_multipeople.ipynb```

A simulatiton setup and strategy, descriptions of the MPC and reference controller, result comparisons and discussions can be found in Section 6. Simulation Platform and Test Results in the document.

For discussions for the firt demo file, see Section 6.3. Demonstration of Disturbance Rejection of MPC-Comfort.

For those for the second and third demo files, See section 6.4. Demonstration of Coordinating Individuals Thermal Comfort in an Energy Efficient Manner.




# Authors
* Donghun Kim (donghunkim@lbl.gov)
* Ronnen Levinson (rmlevinson@lbl.gov)

# License
It is licensed under the terms of a [modified 3-Clause BSD License](./LICENSE) and is available for free.

# Links














