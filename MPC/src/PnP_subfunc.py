import numpy
import scipy

def H_vec(a):
    a=numpy.matrix(a)
    [n,m]=numpy.shape(a)
    y=a[:,0]
    for k in range(1,m):
        y=numpy.concatenate((y,a[:,k]))
    if y.shape[0]!=a.size:
        y=y.transpose()
    return y 

def H_obs(A,C,Np): # create observability matrix with Np blk, original name: H_zeroinputmaptoy
    A=numpy.matrix(A)
    C=numpy.matrix(C)
    OBS=C*A # observability matrix y[1],... y[Np]=[CA; CA^2;.. CA^Np]
    for k in range(2,Np+1):
        Ak=numpy.linalg.matrix_power(A,k)
        OBS=numpy.concatenate((OBS,C*Ak))
    return OBS  
    
def H_mar(A,B,C,Np):
# Markov parameter y[1] [CB,                  u[0]
#                  y[2] CAB,CB                u[1]
#                  y[3] CA^(Np-1)B, .... CB]  u[2]     
    A=numpy.matrix(A)
    B=numpy.matrix(B)
    C=numpy.matrix(C)    
    n=A.shape[0]
    p=C.shape[0]
    m=B.shape[1]
    
    M=numpy.matrix(numpy.zeros((p*Np,m*Np)))
    for ithblk in range(1,Np+1):
        for jthblk in range(1,ithblk+1):
            ithblkrng=numpy.arange(((ithblk-1)*p+1),ithblk*p+1)
            jthblkrng=numpy.arange(((jthblk-1)*m+1),jthblk*m+1)   
            M[numpy.ix_(ithblkrng-1,jthblkrng-1)]=C*numpy.linalg.matrix_power(A,ithblk-jthblk)*B
    return M

def H_blk_diag(A):
    if isinstance(A,list):
        m=len(A)
        dum=numpy.array([])
        for i in range(0,m):
            dum=scipy.linalg.block_diag(dum,A[i])
    else:
        error()
    dum=numpy.mat(dum)
    blkdiag=dum[1:,:] # scipy treats empty matrix as zeros when is appended... strange
    return blkdiag
            
 
        
def H_shift(Aold,Bnew,from_direction):     
    (ar, ac)=Aold.shape # r: row
    (br, bc)=Bnew.shape# r: row
    if from_direction=='l':
        C=numpy.concatenate((Bnew,Aold),axis=1);
        C=C[:,0:ac];
    elif from_direction=='r':
        C=numpy.concatenate((Aold,Bnew),axis=1);
        C=C[:,bc:];
    elif from_direction=='u':
        C=numpy.concatenate((Bnew,Aold),axis=0);
        C=C[0:ar,:];
    elif from_direction=='d':
        C=numpy.concatenate((Aold,Bnew),axis=0);
        C=C[br:,:];    
    return C
    
