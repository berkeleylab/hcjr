from __future__ import print_function
from numpy import *
import numpy as np
from matplotlib.pyplot import *
from control.matlab import *
import scipy as sp
import copy
from pandas import *
import datetime
import scipy.signal
#from __future__ import print_function

#def H_import():
#    from numpy import *
#    import numpy as np
#    from matplotlib.pyplot import *
#    from pandas import *

def filtfilt(N,x):
    # scipy.signal.filtfilt(ones((5)),1./5*ones(1),random.random(100))
    x0=x[0]
    y=scipy.signal.filtfilt(ones(N),N*ones(1),H_iscolumn(x).T-x0)+x0
    y=y.reshape(x.shape)
    return(y)
    
    
def H_date2num(dates): # python datetime |---> python datenumber
# dates=date_range('2018-08-01 00:00:00','2018-08-01 00:15:00',freq='15T')
# pythondatenum=H_date2num(dates)
    try:
        phthondatenum=dates.timestamp()
    except:
        phthondatenum=np.zeros((len(dates),1))
        for k in range(len(dates)):
            phthondatenum[k]=Timestamp(dates[k]).timestamp()    
    #phthondatenum=to_datetime(phthondatenum)
    #matplotlib.dates.date2num(Timestamp('2010-01-01'))
    return phthondatenum
    
def H_num2date(phthondatenum): # python datesnumber |---> python datetime 
# dates=date_range('2018-08-01 00:00:00','2018-08-01 00:15:00',freq='15T')
# pythondatenum=H_date2num(dates)
# H_date2num(pythondatenum)
    try:
        phthondatenum=phthondatenum.squeeze()
    except:
        phthondatenum=phthondatenum
    try:
        dates=Timestamp(phthondatenum,unit='s')
    except:
        dates=[Timestamp(numk,unit='s') for numk in phthondatenum]            
    return dates
    
def H_date2simtime(dates,ref): # python datetime |---> H_defined datenumber
# dates=date_range('2018-08-01 00:00:00','2018-08-01 00:15:00',freq='15T')
# simtime=H_date2simtime(dates,'2018-01-01 00:00')
# Timestamp(simtime[0],unit='s')
# H_simtime2date(simtime,'2018-01-01 00:00:00')
    pythondateref=Timestamp(ref)
    simtime=H_date2num(dates)-pythondateref.timestamp()
    return simtime

def H_simtime2date(simtime,ref):
    phthondatenumref=Timestamp(ref).timestamp()
    phthondatenum=simtime+phthondatenumref
    dates=H_num2date(phthondatenum)
    return dates
    
def H_random(N,xl,xu):
    x=xl+np.random.random(N)*(xu-xl)
    x=np.array([x]).T
    return x  

    
def modelica_data_analysis(*varargin,**varargdic):
    if len(varargin)==1:
        res=varargin[0]
        t=res['time']
        Tz=res['y']# 
        Toa=res['weaBus.TDryBul']
        qGh=res['weaBus.HGloHor']
        qConv_Wpm2=res['qConGai_flow.y']
        qRad_Wpm2=res['qRadGai_flow.y']
        qIncSol=(res['roo.bouConExtWin.HDif[1]']+res['roo.bouConExtWin.HDir[1]'])/1e3
        if len(varargdic) is not 0: #varargdic is not None:
            Pmat=varargdic['Pmat']
            Q=res['Q'] #W  (+) heating
            R=res['SC.R']
    else:
        t=varargin[0]
        Tz=varargin[1]
        Toa=varargin[2]
        qGh=varargin[3]
        qConv_Wpm2=varargin[4]
        qRad_Wpm2=varargin[5]
        qIncSol=0*varargin[5]
        if len(varargdic) is not 0:
            Pmat=varargdic['Pmat']
            Q=varargdic['Q'] #W  (+) heating
            R=varargdic['R']
            
    if len(varargdic) is not 0: #varargdic is not None:
        isHeat= Q>0
        isCool= Q<=0
    
        Qh=Q[isHeat]
        Ph=np.array(Qh)*np.array(R[isHeat])*Pmat[0,1]
        th=t[isHeat]
        Eh=np.trapz(Ph,t[isHeat])
    
        Qc=-Q[isCool]
        Pc=np.array(Qc)*np.array(R[isCool])*Pmat[0,0]
        tc=t[isCool]
        Ec=np.trapz(Pc,t[isCool])
        return t,Tz,Toa,qGh,qConv_Wpm2,qRad_Wpm2,R,Qh,Ph,th,Eh,Qc,Pc,tc,Ec,Q,qIncSol
    else:
        return t,Tz,Toa,qGh,qConv_Wpm2,qRad_Wpm2,qIncSol



def H_filter(b,a,x):
    (n,m)=x.shape    
    if n<m:
        error()
    ICoffset=np.kron(x[0,:],np.ones((n,1)))
    X=x-ICoffset
    dum=list()
    b=np.squeeze(b)
    for ic in range(m):
        try:
            dum.append(sp.signal.filtfilt(b,a,X[:,ic].T))
        except:
            dum.append(sp.signal.filtfilt(b,a,X[:,ic]))   
    Xf=np.mat(np.vstack(dum).T)
    xf=Xf+ICoffset;
    return xf

def H_offset(x): # offset with column average like matlab
    (n,m)=x.shape
    xs=x-np.kron(np.ones((n,1)),np.mean(x,axis=0))
    return xs

def H_iscolumn(x):
    
    if x.ndim is 1:
        x=x.reshape((len(x),1)) # single to two 2
        #error('singlesize')
    nm=x.shape   
    n=nm[0]
    m=nm[1]

    if n<m:
        xc=x.T
    else:
        xc=x;
    return xc
    
def H_shift(x,opt):
    import copy
    xnew=0*copy.copy(x)
    if opt is 'd':
        xnew[:-1]=x[1:]
    elif opt is 'u':
        xnew[1:]=x[:-1]
    else:
        error('')
        
    return xnew
        

def H_c2dz(A,B,C,D,T): # just zero-order holder
    n=A.shape[0];
    invA=np.mat(np.linalg.inv(A));
    expAT=sp.linalg.expm(A*T);
    Aint=invA*np.mat(expAT-np.eye(n));
    Ad=expAT;
    Bd=Aint*B;
    Cd=C;
    Dd=D;
    return (Ad,Bd,Cd,Dd)

def H_c2dm(A,B,C,D,T): # given Continous time (A,B,C,D)
    # if I change setpoint at [k] for a perfectly controlled system at x[k]
    # 1. I am interested in energy [k,k+1), not power at [k+1]
    # this means my output is int(y_continous)
    # 2. since state would change during [k,k+1), I need somewhat integral form for C & D 

    n=A.shape[0];
    invA=np.mat(np.linalg.inv(A));
    expAT=sp.linalg.expm(A*T);
    Aint=invA*np.mat(expAT-np.eye(n));
    Ad=expAT;
    Bd=Aint*B;
    Cm=1./T*C*Aint;
    Dm=C*invA*(1/T*Aint-np.eye(n))*B+D;
    return (Ad,Bd,Cm,Dm)
    
def H_controller(y,r,Ierr,ctrltype,*varargin):
    err=np.mat(y)-np.mat(r);
    Ierr=err+np.mat(Ierr);
    if ctrltype is 'PI':
        Kp=varargin[0];
        Ki=varargin[1];
        u=Kp*err+Ki*Ierr;
    elif ctrltype is 'ONOFF':
        if err>0.5:
            u=-np.mat(5);
        elif err<-0.5:
            u=np.mat(0);
        else:
            u=varargin[1];
    return u,Ierr   

def H_RC(Ts_hr,**varargin_key):
    Ts_min=Ts_hr*60
#    from control.matlab import *
#    from Grey_Structure1z import *
    from Grey_Structure1z import Grey_Structure1z
    import copy
    cc=concatenate
    mm=matmul
    descript=dict()
    
    if len(varargin_key)==0:
        theta=np.array([3.987,  1.183,       0.4788,    29.38,   2.845,    0]);
    #      Cw kWh/oC, Cz kWh/oC, Rzw oC/kW, Roz oC/kW, As m^2, p [-].
    (A, B, C, D)=Grey_Structure1z(theta,1)
    # Qheatingcap, Toa, solar, Qheatgain
    
    descript['theta']='Cw,Cz,Rzw,Roz,As,p'
    descript['x']='Tz,Tw'
    descript['u']=['Qheat(+)','Toa','Qsol','Qg']
    G=ss(A,B,C,D)    
    # creat discrete model from the continous model with sampling time of 5 min
    dsys=c2d(G,Ts_min*1./60,method='zoh')
    import matplotlib.pyplot as pp
    pp.figure(1)
    for iu in range(B.shape[1]):
        pp.subplot(1,B.shape[1],iu+1)
        [yc,tc]=step(G[0,:],np.linspace(0,2),input=iu)
        [yd,td]=step(dsys[0,:],np.arange(0,2,Ts_min*1./60),input=iu)
        pp.plot(tc,yc,'r')
        pp.hold(True)
        pp.plot(td,yd,'o-')
        pp.xlabel(['hr'])
        pp.ylabel([descript['u'][iu]])

    dsys.dt=1.; # change time unit to step
    print(descript.keys())
    for k  in descript.keys():
        print(k+':'+str(descript[k]))
        
    return G,dsys,theta

def H_estimateRo(Tz,Toa,Qh,Ts_hr):
    N=Tz.shape[0]
    Windowsize=int(3*24*1./Ts_hr)
    Nblk=int(N*1./Windowsize)
    TZ=Tz[:Windowsize*Nblk]
    TOA=Toa[:Windowsize*Nblk]
    QH=Qh[:Windowsize*Nblk]
    I=np.eye(Nblk)
    avg=1./Windowsize*np.ones((1,Windowsize))
    Tavg=np.kron(avg,I)
    Tzavg=np.mat(Tavg)*np.mat(TZ)
    Toavg=np.mat(Tavg)*np.mat(TOA)
    Qhavg=np.mat(Tavg)*np.mat(QH)
    figure(1001)
    plot(Tzavg-Toavg,Qhavg,'o')
    xlabel(['Tz-To [^oC]'])
    ylabel(['Qh [kW]'])
    PHI=np.hstack((Tzavg-Toavg,np.ones((Nblk,1))))
    theta=np.linalg.inv(PHI.T*PHI)*PHI.T*Qhavg
    Roa=1./theta[0]
    Qh0=theta[1]
    return Roa, Qh0

def H_schedule(tsec,ON_period,ON_value,OFF_value):
    hr=tsec*1./3600%24
    schedule=ON_value*((ON_period[0]<=hr) & (hr<=ON_period[1]))+OFF_value*((hr<ON_period[0]) | (hr>ON_period[1]))
    schedule=H_iscolumn(schedule)    
    return schedule    
    #step(tsec*1./3600,y,where='post')

def H_plots(x,y,*varargin):
    from H_splot import H_splot
    Ys=H_splot(x,y,varargin)
    return Ys
    
def H_plotc(y1,y2):
    miny=min(min(y1),min(y2))[0,0]
    maxy=max(max(y1),max(y2))[0,0]
    plot(y1,y2,'o');#hold(True);
    
    t=linspace(min(miny,0),maxy,20)
    
    plot(t,t,'k-'); #hold(True);
    plot(t,t*1.1,'--');#hold(True);
    plot(t,t*0.9,'--')  
    
    xlim([min(miny,0),maxy])
    ylim([min(miny,0),maxy])
    xlabel('y1')
    ylabel('y2')
    grid(True)
    
    

def H_bound(x,l,u):
    n=x.size
    xcut=x
    if type(l) is float or type(u) is int:
        for i in range(n):
            xcut[i]=min(max(x[i],l),u)
    else:
        for i in range(n):
            xcut[i]=min(max(x[i],l[i]),u[i])

    return xcut
    
#%%==============================================================================
# Customized data manipulation for DataFrame obj    
#==============================================================================

def H_rplot(df,month,**varargindic): # representative plot
    # check df is dataframe
    # check index is datetime obj
    m=len(df.columns)
    df=df[df.index.month==month] # select e.g. Aug data
    df['hr']=df.index.hour # create hr column for subgrouping
    grouped=df.groupby('hr')
    hrlymean=list()
    hrlymax=list()
    hrlymin=list()
        
    for ihr in range(0,24):
        hrlymean.append(grouped.get_group(ihr).mean().values)
        hrlymax.append(grouped.get_group(ihr).max().values)
        hrlymin.append(grouped.get_group(ihr).min().values)
    dmean=vstack(hrlymean)
    dmax=vstack(hrlymax)
    dmin=vstack(hrlymin)
        
    figure()
    plot(dmean[:,-1],dmean[:,0:-1],'o-');
    if len(varargindic)!=0:
        plot(dmean[:,-1],dmax[:,0:-1],'--');
        plot(dmean[:,-1],dmin[:,0:-1],'--');
                
    xlim([0,24]);xlabel('hr');
    legend(arange(1,m+1).tolist())
    grid(True)
    return dmean, dmax, dmin

def H_fitlab(x,f):
    xmin=min(x)
    xmax=max(x)
    x=xmin+(xmax-xmin)*random.random((100,1))
    y=f(x)
    plot(x,y,'o')
    grid(True)    
    
def H_fitlab2(xmin,xmax,f):
    n=size(xmin)
    N=1000
    x=mat(zeros((N,n)))

    for i in range(n):
        x[:,i]=xmin[i]+(xmax[i]-xmin[i])*mat(random.random((N,1)))
    y=f(x)
    names=list('y')
    for i in range(n):
        names.append('x'+str(i))
    df=DataFrame(hstack((y,x)),columns=names)
    plotting.scatter_matrix(df)
#%%==============================================================================
# unit conversion
#==============================================================================


def ton2kW(Qton):
    QkW=3.5168525*Qton
    return QkW
    
def kW2ton(QkW):
    Qton=.284345136*QkW
    return Qton

def c2f(Tc):
    Tf=9./5*Tc+32
    return Tf
def f2c(Tf):
    Tc=(Tf-32)*5./9
    return Tc
    
def k2c(Tk):
    Tc=Tk-273.15
    return Tc

def c2k(Tc):
    Tk=Tc+273.15
    return Tk

def kJ2kWh(PkJ):
    # 1kWh def=1kW*3600 sec
    PkWh=PkJ*1./3600
    return PkWh

def kWh2kJ(PkWh):
    PkJ=3600*PkWh
    return PkJ    
def ton2kJphr(Qton):
    QkJphr=12660.67023144*Qton;
    return QkJphr
    
def kJphr2ton(QkJphr):
    Qton=QkJphr*1./12660.67023144;
    return Qton
    
def kJ2tonhr(QkJ):
    # 1kJ= 1kW*1 sec = 1/3.5 ton*1./3600hr ==> 1kJ= 1/3.5*1/3600 [ton-hr]
    Qtonhr=kW2ton(QkJ)*1./3600;
    return Qtonhr
    
def tonhr2kJ(Qtonhr):
    QkJ=12660.67023144*Qtonhr;
    return QkJ
    
def cfm2kgs(cfm):
    m3s=0.00047194745*cfm;
    air_density=1.2;
    kgs=air_density*m3s;
    return kgs;
    
def kgs2cfm(kgs):
    air_density=1.2;    
    m3s=kgs/air_density;
    cfm=m3s/0.00047194745;
    return cfm;
    
    
def kgs2gpm(mkgs):
    rhow=1000;
    Vgpm=15850.32314*mkgs/rhow        
    return Vgpm

def gpm2kgs(Vgpm):    
    rhow=1000;
    mkgs=rhow*1./15850.32314*Vgpm
    return mkgs

def lbm2kg(mlb):
    mkg=0.453592*mlb
    return mkg
    
def kg2lbm(mkg):
    mlbm=mkg*1./0.453592
    return mlbm
    
def lbmhr2kgs(mlbmhr):
    mkgs= lbm2kg(mlbmhr)/3600
    return mkgs
    
def kgs2lbmhr(mkgs):
    mlbmhr=kg2lbm(mkgs)*3600
    return mlbmhr        
    
def BTUphrft2F2Wpm2K(h_BTUpft2F):
    h_Wpm2C=h_BTUpft2F*5.678263
    return  h_Wpm2C

def Wpm2K2BTUphrft2F(h_Wpm2C):
    h_BTUpft2F=h_Wpm2C*1./5.678263
    return h_BTUpft2F

def ft2m(Lft):
    Lm=Lft*0.3048
    return  Lm
    
def m2ft(Lm):
    Lft=Lm*1./0.3048
    return Lft
    
    
def ft22m2(Aft2):
    Am2=0.092903*Aft2
    return Am2

def BTUphr2W(Q_BTUphr):
    Q_W=0.29307107*Q_BTUphr
    return Q_W    

def W2BTUphr(Q_W):
    Q_BTUphr=Q_W*1./0.29307107
    return Q_BTUphr    

def myfunc(x):
    y=x**2
    return y
    
def myfunc2(x):
    y=mat(zeros((x.shape[0],1)))
    y=np.multiply(x[:,0],x[:,0])+np.multiply(x[:,1],x[:,1])
    return y
    
def test(a,*varargin, **var_key_n_arg):
    
    print(type(varargin))
    print(type(var_key_n_arg))
    print(varargin is None)
    print(var_key_n_arg is None)
    for key, value in var_key_n_arg.iteritems():
        print("%s == %s" %(key,value))
#%%
if __name__ is '__main__':
    test(1,3,b=3)
    #y=H_iscolumn(np.linspace(0,10,11))
    H_RC(Ts_hr=1)
    #H_plotc(Y,hatY)
    figure(2)
    H_fitlab(np.array([1,2]),myfunc)
    
    figure(3)
    H_fitlab2(np.array([-1,-1]),np.array([1,1]),myfunc2)
    