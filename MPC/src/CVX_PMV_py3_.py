#==============================================================================
# Problem definition
# given A, B, C, K with B=[Bc,Bw] where Bc in R^{n,m}, Bw in R^{n,mw} s.t.
# x=Ax+Bc*u+Bw*w+K*(y-haty)
# y=Cx (y in R^p)
# and given Pmat0 in R^m, lb0 in R^m, ub0 in R^m
# min R(1)[Pmat0_1*u_1(1)+Pmat0_2*u_2(1)]+R(2)[Pmat0_1*u_1(1)+Pmat0_2*u_2(1)]+wz*z+w*v_u+w*v_l
# Yl-v_l<=Ox0+Mu+Mww<=Yu+v_u
# R(k)[Pmat0_1*u_1(k)+Pmat0_2*u_2(k)]\le z \forall k in Nblk
#%% copied & modified from "H_Upper_MPC_V1_wtQg_CVXpy.py"
#==============================================================================
import cvxpy as cp
import numpy as np
import matplotlib.pyplot as mp
import scipy as sp
from H_blkdiag import H_blkdiag
#from PnP_subfunc import PnP_subfunc
import PnP_subfunc

class CVX_PMV:
    def __init__(obj,A,B,C,K,Np,Nblk,Pmat0,lb0,ub0,PMVtheta,**varargindic):
        obj.p=C.shape[0]
        obj.m=Pmat0.size
        obj.n=A.shape[0]
        obj.mw=B.shape[1]-obj.m
        
        obj.a=np.mat(A)
        obj.bc=np.mat(B)[:,0:obj.m]
        obj.bw=np.mat(B)[:,obj.m:] # <<< modification from [:,-1] 
        obj.c=np.mat(C)
        obj.k=np.mat(K)
        obj.theta=PMVtheta # PMV[-3~3]=theta[0]+theta[1]Tz+theta[2]TMRT
        
        obj.Np=Np
        
        
        obj.Obs=PnP_subfunc.H_obs(obj.a,obj.c,obj.Np)
        obj.Mcs=PnP_subfunc.H_mar(obj.a,obj.bc,obj.c,obj.Np)
        obj.Mws=PnP_subfunc.H_mar(obj.a,obj.bw,obj.c,obj.Np)
        
        # blocking +++++++++++++++++++
        obj.Nblk=Nblk
        if obj.Np%obj.Nblk !=0:
            error()
        obj.Tr={} # define empty dictionary
        obj.Tr['bu2pu']=H_blkdiag(np.kron(np.ones((int(obj.Np/obj.Nblk),1)),np.eye(obj.m)),obj.Nblk)
        obj.Tr['bw2pw']=H_blkdiag(np.kron(np.ones((int(obj.Np/obj.Nblk),1)),np.eye(obj.mw)),obj.Nblk)
        obj.Tr['by2py']=H_blkdiag(np.kron(np.ones((int(obj.Np/obj.Nblk),1)),np.eye(obj.p)),obj.Nblk)
        
        # Transformation from [u,v]^T--> [u,...,u,v,...,v]^T        
        obj.Tr['yavg']=H_blkdiag(np.kron(np.ones((1,int(obj.Np/obj.Nblk))),np.eye(obj.p))/(int(obj.Np/obj.Nblk)),obj.Nblk)# averaging y
        obj.Tr['wavg']=H_blkdiag(np.kron(np.ones((1,int(obj.Np/obj.Nblk))),np.eye(obj.mw))/(int(obj.Np/obj.Nblk)),obj.Nblk)# averaging w
        obj.Tr['uavg']=H_blkdiag(np.kron(np.ones((1,int(obj.Np/obj.Nblk))),np.eye(obj.m))/(int(obj.Np/obj.Nblk)),obj.Nblk)# averaging w for different rates for e.g. cooling & heating
        obj.Tr['savg']=H_blkdiag(np.kron(np.ones((1,int(obj.Np/obj.Nblk))),np.eye(1))/(int(obj.Np/obj.Nblk)),obj.Nblk)# averaging y
            
        
        # state responses not output responses
        obj.Obsx=PnP_subfunc.H_obs(obj.a,np.mat(np.eye(len(obj.a))),obj.Np)
        obj.Mcsx=PnP_subfunc.H_mar(obj.a,obj.bc,np.mat(np.eye(len(obj.a))),obj.Np)
        obj.Mwsx=PnP_subfunc.H_mar(obj.a,obj.bw,np.mat(np.eye(len(obj.a))),obj.Np)
        
        
        obj.Ob=obj.Tr['yavg']*obj.Obs;
        obj.Mc=obj.Tr['yavg']*obj.Mcs*obj.Tr['bu2pu'];
        obj.Mw=obj.Tr['yavg']*obj.Mws*obj.Tr['bw2pw'];
        
        obj.PnPCount=obj.Np
        obj.yPs=np.mat(np.zeros((obj.p,obj.Np)))
        obj.Us=np.mat(np.zeros((obj.m,obj.Np))) #  [u(0),u(1),u(2),...]
        obj.Pmat0=np.mat(Pmat0)
        obj.lb0=np.mat(lb0)
        obj.ub0=np.mat(ub0)
        obj.lb=np.concatenate((np.tile(obj.lb0,(1,obj.Nblk)),np.zeros((1,3))),1)
        obj.Pmat=np.tile(obj.Pmat0,(1,obj.Nblk))
        obj.flag=0 # exit flag, 1: LIP -1:LP -2:Conv
#==============================================================================
        obj.w_on_d=0.001 # weight on demand Energy+weight*Demand
#==============================================================================
        obj.iter=0
        obj.hatxk=np.mat(np.zeros((obj.n,1)))
        obj.inno=np.mat(np.zeros((obj.p,1)))
        
        obj.verbose=False
        
        if len(varargindic)==0:
            obj.comfmetric='max'
        else:
            obj.comfmetric=varargindic['comfmetric']
#        obj.ybank=np.mat(np.zeros((6,obj.p))) # for tracking mean state [y1.T;y2.T;y3.T...]
#        obj.ubank=np.mat(np.zeros((6,obj.p)))
#        obj.wbank=np.mat(np.zeros((6,1)))
    

    def Kestim(obj,yk,uk,wk): # I am interested in mean behaviour rather than zigzag		
        # xk+|k=Axk|k-1+Buk+K(yk-Cxk|k-1)
        hatxmk=obj.hatxk   				# time averaged state
        
        # converts to mean average behaviour
#        obj.ybank=H_shift(obj.ybank,yk.T)
#        obj.ubank=H_shift(obj.ubank,uk.T)
#        obj.wbank=H_shift(obj.wbank,wk.T)
#        ymk=np.mat(np.mean(obj.ybank,axis=0)).T
#        umk=np.mat(np.mean(obj.ubank,axis=0)).T
#        wmk=np.mat(np.mean(obj.wbank,axis=0)).T
#        print obj.ybank.T
#        print obj.ubank.T
#        print obj.wbank.T
        ymk=yk #H_vec(yk)
        umk=uk
        wmk=wk
        		
        inno=np.mat(ymk-obj.c*hatxmk)
        obj.inno=inno
        hatxmkp1=np.mat(obj.a*hatxmk+obj.bc*umk+obj.bw*wmk+obj.k*inno)
        #hatxmkp1[0:-1*obj.p]=0 # enforce it as P (NOT I!) controller
        obj.hatxk=hatxmkp1
        # if obj.verbose:
            # print('current mean temps are ', hatxmk.T)
            # print('innovation is : ',inno)
            
    def UMPC(obj,yk,YLs,YUs,Ws,Rs): # YL,YU,W,R are matlab row vector -types
        if obj.verbose:
            print('# CONTROL DECISION STARTS==================================================\
            #===================================================================================')  
            print ('the values of ySP and yk should be based on the model, i.e. offset, scale and oF/oC: ',yk)
        
        hatxk=obj.hatxk
		
#        if YLs.shape!=(obj.Np,obj.p):
#            error()
#        if YUs.shape!=(obj.Np,obj.p):
#            error()
        if Ws.shape!=(obj.Np,obj.mw):
            error()
        if Rs.shape!=(obj.Np,obj.m):
            error()
        # Time series to vector form
        YL=YLs.reshape((YLs.size,1),order='C') # scalar time series
        YU=YUs.reshape((YUs.size,1),order='C')             
        W=Ws.reshape((Ws.size,1),order='C')
        R=Rs.reshape((Rs.size,1),order='C')

#        # averaging future info for blocking
#        YUblk=obj.Tr['yavg']*YU;
#        YLblk=obj.Tr['yavg']*YL;
#        Wblk=obj.Tr['wavg']*W;            
#        Rblk=obj.Tr['uavg']*R;
        YUblk=YU;YLblk=YL;Wblk=W;Rblk=R;
        if obj.Np!=obj.Nblk:
            error('I have not tested yet')
                 
        
        if obj.PnPCount<obj.Np:
            yPE=np.linalg.norm(yk-obj.yPs[:,int(obj.PnPCount)-1])
            #print('yPE is: ',yPE)
        else:
            yPE=1e3 # so as to implement PnP
            #print('I already implemented u for Np-time thus run PnP')
            
        if yPE<0 and obj.flag>=0:#====================
            #print('yPE is less than 0.3oF',yPE)
            obj.PnPCount=obj.PnPCount+1
            #print ('PnP is holding and Count is :',obj.PnPCount)
            dum2=obj.Obs*hatxk
            yDs=np.reshape(dum2,(obj.p,obj.Np),'F')                
            #print('predicted dist based on 72 are: ', yDs)                        
            flag=obj.flag                        
            uk=obj.Us[:,(obj.PnPCount-1)]   
        else:
            
            obj.iter=obj.iter+1  
         
            # cvxpy =================================================
            #sp.io.savemat('cvxpytest',{'a':obj.a,'bc':obj.bc,'c':obj.c,'bw':obj.bw})

            YLblks=YLblk.reshape((obj.Nblk,1),order='C') #,<< 1 was originally obj.p. but I used Ybound only for comfort
            YUblks=YUblk.reshape((obj.Nblk,1),order='C')
            Rblks=Rblk.reshape((obj.Nblk,obj.m),order='C')
            Wblks=Wblk.reshape((obj.Nblk,obj.mw),order='C')
            
            
            x=cp.Variable((obj.n,obj.Nblk+1))# x(0)
            r=cp.Variable((obj.p,obj.Nblk)); # r[0]:=y(1), r[Np-1]=y(N)
            u=cp.Variable((obj.m,obj.Nblk))
            vu=cp.Variable(1)
            vl=cp.Variable(1)
            z=cp.Variable(1)
            
            func=0
            constr=[]
            
            if obj.comfmetric is 'max':
                for k in range(obj.Nblk):
                    # physical constraints
                    func +=np.array(Rblks[k,:])*u[:,k]
                    constr +=[np.squeeze(np.array(obj.lb0)) <= u[:,k], 
                              u[:,k]<= np.squeeze(np.array(obj.ub0)), 
                              np.squeeze(np.array(YLblks[k,:]))-vl<=r[1:,k], # comfort lower viol, r[0]:=Tz, r[1]:=PMV-theta[0]
                              r[1:,k]<=np.squeeze(np.array(YUblks[k,:]))+vu, # comfort upper viol
                              Rblks[k,:]*u[:,k] <=z] # demand
                    # dynamic constraints
                    constr +=[ x[:,k+1]==obj.a*x[:,k]+obj.bc*u[:,k]+np.squeeze(np.array(obj.bw*Wblks[k,:].T)), 
                               r[:,k]== obj.c*x[:,k+1]] # temp & PMV
                    
                func +=1e0*z +1e5*vu+1e5*vl
                constr += [x[:,0]==np.squeeze(np.array(obj.hatxk)), 
                           z >= 0, vl >= 0, vu >= 0]
            
            elif obj.comfmetric is 'mean':
                print('obj is : mean')
                for k in range(obj.Nblk):
                    # physical constraints
                    func +=np.array(Rblks[k,:])*u[:,k]
                    constr +=[np.squeeze(np.array(obj.lb0)) <= u[:,k], 
                              u[:,k]<= np.squeeze(np.array(obj.ub0)), 
                              np.squeeze(np.array(YLblks[k,:]))-vl<=0.5*(r[1,k]+r[2,k]), # comfort lower viol, r[0]:=Tz, r[1]:=PMV-theta[0]
                              0.5*(r[1,k]+r[2,k])<=np.squeeze(np.array(YUblks[k,:]))+vu, # comfort upper viol
                              Rblks[k,:]*u[:,k] <=z] # demand
                    # dynamic constraints
                    constr +=[ x[:,k+1]==obj.a*x[:,k]+obj.bc*u[:,k]+np.squeeze(np.array(obj.bw*Wblks[k,:].T)), 
                               r[:,k]== obj.c*x[:,k+1]] # temp & PMV
                    
                
                func +=1e0*z +1e5*vu+1e5*vl
                constr += [x[:,0]==np.squeeze(np.array(obj.hatxk)), 
                           z >= 0, vl >= 0, vu >= 0]
                
             
                
            problem = cp.Problem(cp.Minimize(func), constr)
            problem.solve(solver=cp.ECOS)#,verbose=True)#cp.CVXOPT
            
            #print('PMV: '+ str(r[1:,k].value))            
            #print('mean PMV: '+ str(mean(r[1:,k].value)))
            
            if problem.status in ['optimal', 'optimal_inaccurate']:
                flag=1
                SPk=np.mat(r[0,0].value) # r[0]=y(1)
                UOPTK=np.mat(u[:,0].value)
                print('SP:', SPk.T, 'vu:', vu.value, 'vl:', vl.value)
                
                UOPT=np.mat(u.value.reshape((u.value.size,1),order='F')) # last term is demand, Tu,Tl
                #predict_Np(obj,hatxk,UOPT,YU,YL,W,Rs)
            else:
                print(problem.status)
                SPk=np.mat(r[0,0].value) # r[0,0]=Tz(1), r[1,0]=PMV(1)-theta0
                UOPTK=np.mat(u[:,0].value)
                print('SP:', SPk.T, 'vu:', vu.value, 'vl:', vl.value)
                #error('no convergence??')
            
            #predict_Np(obj,hatxk,UOPT,YU,YL,W,Rs)   
            
            
            
#            input('a')
            # cvxpy end =================================================
            
            
        #print('# CONTROL DECISION WAS MADE==================================================\
        #===================================================================================') 
        
        return (SPk.T,UOPTK.T,flag) #==============================
        
def predict_Np(obj,hatxk,Ublk,YU,YL,W,R):

	
    Uop=obj.Tr['bu2pu']*Ublk
    Yop=obj.Obs*hatxk+obj.Mcs*Uop+obj.Mws*W # [y'[1],y'[2],...,y'[Np]]' %===============
    
    
    ys=np.reshape(Yop,(obj.p,obj.Np),'F')   
    Tzs=ys[0,:]
    PMVs=ys[1:,:]#+obj.theta[0]
    #print(PMVs)
    
    uop=np.reshape(Uop,(obj.m,obj.Np),'F')
    
    
    x=obj.Obsx*hatxk+obj.Mcsx*Uop+obj.Mwsx*W
    
    
    xs=np.reshape(x,(obj.n,int(len(x)/int(obj.n))),'F')
    Tws=xs[1,:]
    t=np.tile(np.mat(range(0,obj.Np)),1).T
             
    Ws=np.reshape(W,(obj.mw,int(W.size/obj.mw)),'F')
    
    mp.figure(101)
    mp.subplot(221)
    mp.plot(t,Tzs.T,linewidth=2)
    
    #mp.plot(t,Ws[0,:].T,'k',linewidth=1)
    mp.grid('on')
    legend(['Tz [^oC]'])
    #ax.fill_between(t, squeeze(np.array(YL), squeeze(np.array(YU), facecolor='blue', alpha=0.5)
    
    ax=mp.subplot(223)
    ax.step(t,uop.T,linewidth=3, where='post')
    ax.legend(np.arange(1,obj.m+1).tolist())
    ax.fill_between(np.squeeze(np.array(t)),uop.min(),uop.max(),where=np.squeeze(np.array(R))>int(min(R)),facecolor='grey',alpha=0.3)
    
    ax.set_ylabel('Q_opt')
    #mp.step(t,R,'g--',where='post')
    ax.grid('on')
    # mp.ylim(-0.1, 3.1) 
    
    
    mp.subplot(222)
    mp.plot(t,xs.T,linewidth=3)
    legend(['Tz [^oC]','Tw [^oC]'])
    #mp.hold(True)
    #mp.plot(t,xs[obj.p:2*obj.p,:].T)
    #mp.hold(False)
    mp.grid('on')
    
    ax=mp.subplot(224)
    ax.plot(t,PMVs.T,linewidth=3)
    ax.fill_between(np.squeeze(np.array(t)),np.squeeze(np.array(YU)),np.squeeze(np.array(YL)),alpha=0.3)
    
    #mp.hold(True)
    #mp.plot(t,np.multiply(R,np.transpose(obj.Pmat0*uop)),label='$')
    #mp.hold(False)
    ax.set_ylabel(['PMV'])
    ax.set_xlabel(['hr'])
    ax.grid('on')
    
    #mp.savefig('test'+time.strftime('%m_%d_%H_%M_%S',time.localtime(time.time()))+'.png')
    mp.show()
    #mp.close('all')
        
    
def H_shift(Y,ynewT):
	if Y.shape[0]==1:
		error('give me matlab like time series')
	dumY=np.roll(Y,-1,0)
	dumY[-1,:]=ynewT
	YNEW=dumY
	return YNEW
 

    


